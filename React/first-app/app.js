// j'enregistre dans la variable title.
const title =  React.createElement('h1', {}, 'Hello Word', 'Jean');

// je rend la variable title avec la méthode render()
ReactDOM.render(title, document.querySelector('#app'));

// j'affiche dans la console.
console.log(title);

// Créer un compteur
let n =1
function render() {
    const title1 = React.createElement('h1', {},
        'Redirection dans :',
        React.createElement('span', {}, n)
    );

    ReactDOM.render(title1, document.querySelector('#app1'));
}

render();

setInterval(() => {
    n++;
    render();
}, 1000);