// j'enregistre dans la variable title.
const title =  React.createElement('h1', {}, 'Hello Word', 'Jean');

// je rend la variable title avec la méthode render()
ReactDOM.render(title, document.querySelector('#app'));

// j'affiche dans la console.
console.log(title);

// Créer un compteur
let n =1

function getString() {
    return n + ' fois';
}
function render() {

    // je veux afficher un tags
    let tags = [
        'Tag1',
        'Tag2',
        'Tag3',
    ];

    // on crée un tableau d'élément.
    const tagsEl = tags.map((tag, index) => {
        return <li key={index}>{tag}</li>
    })
    const title1 = <React.Fragment><!-- si on ne veux pas voir la div, on met juste (<>)ou (<React.Fragment></React.Fragment>) cela s'appelle un fragment -->
        <h1>Compteur : <span>{getString()}</span></h1>
        <ul>{tagsEl}</ul>
    </React.Fragment>

    ReactDOM.render(title1, document.querySelector('#app1'));
}

render();

setInterval(() => {
    n++;
    render();
}, 1000);