/*
let age1 = prompt('quel age avez-vous ?');
console.log(" vous avez",age1,"ans", typeof age1);
alert (age1);
*/
/*
    Questions :
    1. De quand date la première version de Javascript ?
    2. Quel est l'objectif de javascript, à quel besoin réponds-t-il ?
    3. Quel est l'utilisation du javascript ?
    4. Le javascript est-il un langage bas niveau ou haut niveau ? Et pourquoi ?
    5. Qu'est-ce que le typage dynamique ?
    6. Quels sont les différents type de variables ?
    7. Qu'est-ce qu'un polyfill ?
    8. Qu'est-ce qu'un "transpiler" ? Et Citez moi s'en un ou deux transpiller javascript.
    9. Qu'est-ce qu'une instruction et comment sont-elles délimitées ?
    10. Qu'es-ce que la porté d'une variable ?
 */

/*
    Répondre ICI (dans le commentaire)
    à toutes les questions en numérotant et espaçant vos réponses.

    1. La date de la première version de javascript est 1995.
    2. l'objectif de javascript est de dynamiser un site web.
    3. L'utilisation de javascript est côté client et serveur.
    4. Le langage javascript est un langage de haut niveau, c'est un langage de script.
    5. Le typage dynamique est le fait que javascript détecte si la valeur mise dans une variable est de type, number,string,booléen,object.
    6. Les différents type de variables sont number, string, booléen, objet.
    7. Le polyfill est un bout de code permettant de fournir des fontionnalités récente sur un ancien navigateeur.
    8. Un transpiler est un type de compilateur qui prend le code source d'un programme et le compile dans un autres langage. babel ou webpack.
    9. Une instruction est une ligne de code qui se fini par un point-virgule.
    10. La portée d’une variable désigne l’espace du script dans laquelle elle va être accessible.
*/

/*
    Exercice 1 :
    Déclare une variable nommé "age" contenant le nombre 24
    let age = 24;
    Déclare une constante nommé "name" contenant le nom (la chaîne de charactère) de ton choix
    const name = "Bonjour à tous";
*/
//  let age = 24;
//  const name = "Bonjour à tous";

// Exercice 2 : Affiche dans la console de ton navigateur les variables "age" et "name"
// console.log(age, name);

//  Exercice 3 : Multiplie la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 72
//    si utilisé qu'une seule fois on peut faire comme ça
    console.log('exercice 3', age * 3);
//  Exercice 4 : Soustrais la variable "age" avec "age" et affiche le résultat dans la console
//  Résultat souhaité : 0
    let diff = age - age;
    console.log('exercice 4', age - age, diff);
//  Exercice 5 : Divise la variable "age" par 3 et affichez le résultat dans la console
//  Résultat souhaité : 8
//
   console.log('exercice 5', age, age / 3);
/*
    Exercice 6 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" moins 3
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 21
    age  -=  3;// évite la répétition de age.
    console.log('exercice 6',age);
/*
    Exercice 7 :
    - Assigne à la variable "age" (déjà créée) le résultat de la variable "age" multiplié par 10
    - Puis affiche la variable "age" dans la console
*/
//  Résultat souhaité : 210
    age *=10;
    console.log('exercice 7', age);
/*
    Exercice 8 :
    - Déclare une variable "age2" et assigne la valeur 50
    - Puis affiche la variable "age2" dans la console
*/
    let age2 = 50;
    console.log('exercice 8', age2);
/*
    Exercice 9 :
    - Additionne "age" avec "age2"
    - Puis affiche le resultat de l'addition dans la console
*/
//  Résultat souhaité : 260

    console.log('exercice 9', age + age2);
// C'est un exemple pour montrer la marche à suivre sur les prochains exercices.
// Exercice Exemple : Affiche dans la console le résultat de : "age" est-il inférieur à "age2" ?
// Réponsé éxemple : console.log ('Exercice exemple :', age < age2);
    console.log ('Exercice exemple :', age < age2);
// Exercice 10 : Affiche dans la console le résultat de : "age" est-il supérieur à "age2" ?
    console.log ('Exercice 10 :', age > age2);
// Exercice 11 : Affiche dans la console le résultat de : "age2" est-il inférieur à "age" ?
    console.log ('Exercice 11 :', age2 < age);
// Exercice 12 : Affiche dans la console le résultat de : "age2" est-il supérieur ou égale à 50 ?
    console.log ('Exercice 12 :', age2 >= 50);
// Exercice 13 : Affiche dans la console le résultat de : "age" est-il inférieur ou égale à 10 ?
    console.log ('Exercice 13 :', age <= 10);
// Exercice 14 : Affiche dans la console le résultat de : "age" est-il égale à 210 ?
    console.log ('Exercice 14 :', age == 210);
// Exercice 15 : Affiche dans la console le résultat de : "age" est-il strictement égale en valeur et en type à 210 ?
    console.log ('Exercice 15 :', age === 210);
/*
 Exercice 16 - Opérateurs logiques : Écris la phrase suivante et affiche dans la console le résultat.
 Indice : Il vous faut utiliser les opérateurs logiques.
 "
     la variable ex16var1 doit être strictement égale à 215 ET
     la variable ex16var2 doit être vrai ET
     la variable ex16var3 doit être strictement égale à la string "blog".
 "
 Résultat souhaité : true
 */
let ex16var1 = 215;
let ex16var2 = true;
let ex16var3 = "blog";
console.log ('Exercice 16 :',ex16var1 === 215 && ex16var2 === true && ex16var3 === "blog" );
/*
 Exercice 17 - Opérateurs logiques : Écris la phrase suivante et affiche dans la console le résultat.
 Indice : Il vous faut utiliser les opérateurs logiques.
 "
     (la variable ex17var1 multiplié par 2 et diviser par 5 doit être strictement égale à 8 ET
     la variable ex17var2 doit être non égale simple du nombre 5) OU
     la variable ex17var3 doit être égale à faux
 "
 Résultat souhaité : true
 */
let ex17var1 = 20;
let ex17var2 = "5";
let ex17var3 = false;


console.log('Exercice 17 :',(((ex17var1 *2 / 5) === 8) && ex17var2 != 5) || ex17var3 === false);
// Exercice 18 : Déclare une variable de ton choix sans lui affecter de valeur et affiche le résultat dans la console.
let vide;
console.log('Exercice 18 :',vide);
// Exercice 19 : Affiche dans la console le type de toutes les variables déclarées précédemment.
console.log('Exercice 19 :', typeof age, typeof vide, typeof ex17var1, typeof ex17var2, typeof ex17var3);
// Exercice 20 : Ecrire un script dans la balise "head" du fichier index.html de ce dossier qui affiche la chaine de charactères "head"
// <script type="text/javascript">
//      console.log('head');
// </script> mis dans le fichier html dans la balise head
// Exercice 21 : Ecrire un script à la fin de la balise "body" du fichier index.html de ce dossier qui affiche la chaine de charactères "body"
// <script type="text/javascript">
//      console.log('body');
// </script> mis dans le fichier html dans la balise body
// Exercice 22: Créer un fichier app.js dans ce dossier qui affiche dans la console la chaine de charactères "app.js" et importer le dans le fichier index.html de ce dossier
// <script src="app.js"></script>


/*
    Bonus 1 :
    1. Déclarer une variable nommée "bonus" et par défaut assignez-lui le nombre 2
    2. En utilisant un opérateur artihmétique, déterminez si la valeur contenue dans la variable "bonus"
       est paire ou impaire
    3. Assignez le nombre 3 à la variable "bonus"
    4. Comme pour le point 2, affichez dans la console si la variable "bonus" est paire ou impaire

    Dans le point 2 et 4, la façon qui nous permet de déterminer
    si un nombre est paire ou impaire en utilisant un certain opérateur
    est identique.
    Résultat souhaité :
    - Pour le point 2 => 0
    - Pout le point 4 => 1

    Expliquez en quelques lignes ce que représente 0 et 1
    ce que cela traduit de la valeur contenue dans la variable "bonus"
 */
let bonus = 2;
console.log('bonus :', bonus, bonus % 2);// pour tester la parité.
bonus = 3;
console.log('bonus :', bonus,  bonus % 2);// pour tester la parité.
/*
    Bonus 2 :
    Complétez le bonus 1, en y ajoutant une condition qui affiche dans la console :
    - "Nombre paire" si la valeur contenue dans la variable "bonus" est paire
    - "Nombre impaire" si la valeur contenue dans la variable "bonus" est impaire
 */
if (bonus % 2) {
    console.log(bonus, 'impaire');
} else {
    console.log(bonus, 'pair');
}