/*
    Les tableaux :
    Découvrez comment manipuler des tableaux.
    Vous trouverez dans la documentation des méthodes utiles
    pour réaliser les objectifs décrits après.
    Prenez votre temps pour lire la description et compendre l'utilisation des méthodes
    et n'hésitez pas à vous entraider.

    Lorsque je vous demande d'afficher une donnée, il faut l'afficher dans la console.

    Conseil : n'hésitez pas à tester en ajoutant ou modifiant la valeur des variables.

    Documentation : https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array
 */

/**
 * Exercice 1 :
 * Créez un tableau vide dans une variable ou constante
 */
 let arr =[];
/**
 * Exercice 2
 * Créez un tableau dans une constante avec des valeurs initiales : "orange", "red", "pink", "blue"
 */
const arr1 =["orange", "red", "pink", "blue"];
console.log(arr1);
/**
 * Exercice 3
 * Créez un tableau dans une variable avec un maximum de 10 entrées et remplie du boolean "false"
 */
let arr2 = new Array(10);
arr2.fill(false);
console.log(arr2);
/**
 * Exercice 4
 * Créez un tableau dans une variable et ajoutez-y plusieurs valeurs de votre choix
 */
let arr3 = [];
arr3.push(20, 50, 100, 250);
console.log(arr3);
/**
 * Exercice 5
 * Créez un tableau dans une variable et affichez la deuxième valeur dans la console
 */
let arr4 = [];
arr4.push(20, 50, 100, 250);
console.log(arr4[1]);
/**
 * Exercice 6
 * Créez un tableau dans une variable, ajoutez-y 10 éléments de votre choix et supprimez la deuxième valeur
 */
let arr5 = [];
arr5.push(20, 50, 100, 250, 25, 30, 21, 15, 63, 89);
console.log('arr5', arr5.splice(1,1), arr5);
/**
 * Exercice 7 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la première valeur
 */
let arr6 = [];
arr6.push(120, 520, 10, 2540, 5, 370, 2, 175, 663, 9);
console.log('arr6', arr6.splice(0,1), arr6);
/**
 * Exercice 8 :
 * Créez un tableau dans une variable et ajoutez-y 10 éléments de votre choix et supprimez la dernière valeur
 */
let arr7 = [];
arr7.push(120, 520, 10, 2540, 5, 370, 2, 175, 663, 9);
console.log('arr7', arr7.splice(length-1,1), arr7);
/**
 * Exercice 9 :
 * Créez un tableau dans une variable nommée "mat" contenant 2 tableaux vide
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 0
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 1
 * Ajoutez dans le deuxième tableau du tableau "mat" le nombre 2
 * Ajoutez dans le premier tableau du tableau "mat" le nombre 3
 * Affichez le résultat dans la console
 */
const mat = [[],[]];
mat[0].push(0);
mat[1].push(1);
mat[1].push(2);
mat[0].push(3);
console.log('mat', mat);
/**
 * Exercice 10 : Bonus "Immutabilité"
 * Recherchez et décrivez ce qu'est "l'immutabilité"
 * Pourquoi il faut faire attention à l'immutabilité lorsqu'on manipule des tableaux
 */
let arr10 = [2];

//let arr11 = arr10; copie la référence du tableau arr10 dans arr11, si on supprime une valeur dans arr11, on supprimera de faite cette valeur dans arr10. Ce qui pose un problème.il ne faut pas copier le tableau mais le créer va le spread opérateur.
let arr11 = [...arr10]; //avec les trois petits points (spread) on dit que l'on va étaler le tableau arr10. Là je copie les valeur de arr10 dans un nouveau tableau.

let arr12 = [...arr11]; // Crée un nouveau tableau

console.log('exercice10', arr10, arr11);
