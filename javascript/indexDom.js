/*
    Exercice :

    Dans le champ "Le nom de la tâche" entrez le nom d'une tâche que vous souhaitez ajouter
    Lorsqu'on clique sur le bouton "Ajouter"
    Créer un nouvel élément "div" qui a la class "task" et le text de l'élément créé correspond à celui entré dans l'input
    lorsqu'on clique sur l'élément créé je veux qu'il soit supprimé du tableau et du html
    Insérer l'élément dans la div qui a l'id "tasks"

     Ajouter les tâches présentent dans le tableau "tasks" dans la div qui a l'id "tasks" comme précédement

     Lorsqu'on clique sur le bouton "Enregistrer", je veux que vous m'affichiez toutes les tâches qui n'ont pas été supprimé
 */

let tasks = [
    'Sortir le chien',
    'Nettoyer le salon',
    'Jetter les poubelles'
];
