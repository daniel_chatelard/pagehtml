##  <div style="color: #26B260">**XI. API**</div>
[Retour au sommaire](sommaire.md)

### <span style="color: #aaffff">**1. Présentation.**</span>
Une API, pour Application Programming Interface, permet d'utiliser les ressources, données ou fonctionnalités, d'une application web depuis une autre application.
C'est une interface qui permet à deux programmes de communiquer entre eux.<br>

![img_28.png](img_28.png)<br>

### <span style="color: #aaffff">**2. HTTP.**</span>
Veux dire Hypertext Transfert Protocol.C'est un protocole sans état, elle ne sauvegarde pas l'état de la connection http.<br>
Utilisé pour communiquer entre le navigateur et le serveur web.<br>
Le navigateur va envoyer une requête http au serveur, le serveur va renvoyer une réponse.<br>

![img_29.png](img_29.png)<br>

Comment est constitué une requête http?<br>
En premier URL où on va faire la requête(Request URL: http://)<br>
En second (Request Method: GET), quel est le type de notre requête.<br>
![img_30.png](img_30.png)<br>

Les verbes http.<br>
GET pour obtenir.<br>
POST pour soumettre.<br>
PUT pour sauvegarder.<br>
DELETE pour supprimer.<br>
![img_31.png](img_31.png)<br>

Pour créer un utilisateur, on va utiliser la méthode POST. Ces données seront envoyées au serveur.<br>

Pour lire les données d'un utilisateur, on va utiliser la méthode GET pour récupérer des informations de l'utilisateur.<br>

Pour mettre à jour l'utilisateur, on utilise la méthode PUT.<br>

Pour supprimer l'utilisateur, on utilise la méthode DELETE.<br>

En troisième, le status code apparait une fois la réponse effectuée, 200 réponse avec succès.<br>

Request Headers, ce sont des informations que l'on met lorsque l'on va requêter le serveur.<br>

Response Headers, seront lorsque le serveur aura renvoyé les données, ce sont des informations du serveur.<br>

Dans l'onglet Response de la console du navigateur, nous avons la réponse du serveur, Preview permet de voir les données.<br>

On peux avoir le code les plus courant sur le site Wikipédia.<br>
![img_32.png](img_32.png)<br>
![img_33.png](img_33.png)<br>
![img_34.png](img_34.png)<br>

### <span style="color: #aaffff">**3. Logiciel pour tester une api.**</span>
On test les route avec un logiciel comme par exemple insomnia ou postman.<br>
Ils permettent de faire des requêtes.<br>

Crée un espace de travail.<br>
![img_35.png](img_35.png)

Rajouté une nouvelle requête.<br>
![img_36.png](img_36.png)<br>
![img_37.png](img_37.png)<br>

Ensuite, il faut lui mettre l'URL de l'api, puis la méthode.<br>
![img_38.png](img_38.png)<br>

Et enfin choisir le type de format.<br>
![img_39.png](img_39.png)<br>

Comment faire une requête sur une api?<br>
![img_40.png](img_40.png)<br>
On copie l'adresse http dans le curl.<br>
On a en premier l'origine de l'adresse (http:// api-adresse.data.gouv.fr) puis la route (/search/) et les query params qui se nomme (q) (?q=8+bld+du+port).<br>
![img_43.png](img_43.png)<br>
On peut lui ajouter une limit avec le query params (&limit).<br>
![img_41.png](img_41.png)<br>
On aura seulement que 15 réponses.<br>
![img_42.png](img_42.png)<br>
Exemple :<br>
![img_44.png](img_44.png)<br>
On peut aussi le faire dans l'onglet Query.<br>
![img_45.png](img_45.png)<br>
Liste des attributs.<br>
![img_46.png](img_46.png)<br>

Il peut avoir des APIs public ou privée, pour les API privées il faut demander un accès à l'API.<br>

### <span style="color: #aaffff">**4. Première api en NodeJs natif.**</span>
Ce n'est pas très beau et c'est pas ce qu'il y a de mieux.<br>


### <span style="color: #aaffff">**5. Présentation et installation de ExpressJs.**</span>
Cette librairie est très utilisée avec Angular, React, bon nombre de projets.<br>
Allez sur le site expressjs.com dans mise en route.<br>
![img_47.png](img_47.png)<br>

L'onglet Guide.<br>
![img_48.png](img_48.png)<br>

Installation d'express.<br>
![img_49.png](img_49.png)<br>
Créer un dossier express, se mettre dedans puis faire l'initialisation en tapant (npm init dans le terminal)<br>
![img_50.png](img_50.png)<br>

Dans le terminal.<br>
![img_51.png](img_51.png)<br>

Tapez sur entré pour répondre aux question, puis il va initialiser un fichier (package.json).<br>
![img_52.png](img_52.png)<br>

Installer la dépendance express avec --save.<br>
![img_53.png](img_53.png)<br>

ça va rajouter un dossier node_modules et un fichier (package-lock.json).<br>
![img_54.png](img_54.png)<br>

Créer un fichier .gitignore, pour ne pas envoyer sur git le dossier node_module et idea.<br>
![img_55.png](img_55.png)<br>

### <span style="color: #aaffff">**6. Créer un server http avec ExpressJs.**</span>

Crée un nouveau fichier nommé index.js, puis récupérer la librairie express avec le mot clé (require).<br>
![img_56.png](img_56.png)<br>

Créer un serveur dans le fichier index.js, on invoque express dans la const server.<br>
![img_57.png](img_57.png)<br>

Pour activé le serveur on va utiliser (.listen(le port, et une fonction callback)).<br>
![img_58.png](img_58.png)<br>

Pour executer le serveur, on va dans le terminal et on tape (node index.js)<br>

Comment indiquer une route en GET?<br>

Sur notre serveur on va lui indiquer la méthode (.get(URL, fonction callback avec la request, response))<br>
![img_59.png](img_59.png)<br>

On peut renvoyer un titre avec h1.<br>
![img_60.png](img_60.png)<br>

On rajoute une deuxième route ('/users').<br>
![img_61.png](img_61.png)<br>

On rajoute une route post('users').<br>
![img_62.png](img_62.png)<br>
Sur Insomnia.<br>
![img_63.png](img_63.png)<br>

### <span style="color: #aaffff">**7. Paramètre de route et contenue de la requête.**</span><br>
![img_64.png](img_64.png)

Pour accéder à la propriété de request, je dois utiliser body, il faut implémenter un middleware.<br>
![img_65.png](img_65.png)<br>
![img_66.png](img_66.png)<br>
Maintenant, j'ai accès au contenu de ma requête.<br>
![img_67.png](img_67.png)<br>

De cette manière, je peux récuperer le contenu d'une requête.<br>
![img_68.png](img_68.png)<br>
![img_69.png](img_69.png)<br>
Grâce aussi au middleware.<br>
![img_70.png](img_70.png)<br>

Cas où, on souhaite supprimer un route (users/:id).<br>
:id, indique un paramètre dans l'URL.<br>
![img_72.png](img_72.png)<br>
On voit que user récupère bien l'id dans l'URL.<br>
![img_73.png](img_73.png)<br>

Mettre à jour un utilisateur avec la méthode put.<br>
![img_74.png](img_74.png)<br>

On fait dans Insomnia une requête put avec l'URL et la route (/users/) puis l'id (234) et on met à jour via l'onglet JSON les données.<br>
![img_75.png](img_75.png)


### <span style="color: #aaffff">**8. Gérer les erreurs et changer le status de la réponse.**</span>

Comment changer les status de la requête.<br>
![img_76.png](img_76.png)<br>

Dans Insomnia.<br>
![img_78.png](img_78.png)<br>

Une autre façon.<br>
![img_77.png](img_77.png)<br>

Dans Insomnia.<br>
![img_79.png](img_79.png)<br>

Vérifier que l'id corresponds à un utilisateur.<br>
![img_80.png](img_80.png)<br>

Dans Insomnia.<br>
![img_81.png](img_81.png)<br>

