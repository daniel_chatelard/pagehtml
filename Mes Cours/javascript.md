##  <div style="color: #26B260">**VII. JavaScript**</div>
[Retour au sommaire](sommaire.md)

### <span style="color: #aaffff">**Présentation**</span>
javascript est un langage de script,le premier moteur javascript  était spydermankey crée en 1995 par Brendan Eich pour la socièté nescape navigator, l'objectif est de dynamiser les sites web grâce aux interaction et des animations. Anciennement appelé lavscript, il s'utilise (principalement) côté client mais aussi côté serveur.<br/>
Son évolution est gérée par le groupe ECMA International qui se charge de publier les standards de ce langage.

Le JavaScript est un langage dynamique, c'est un langage interprété et orienté objet.

### <span style="color: #aaffff">**Polyfill**</span>
Un polyfill est un bout de code permettant de fournir des fonctionnalitées récente sur un ancien navigateur qui ne les supportent pas nativement.

### <span style="color: #aaffff">**Transpiler**</span>
C'est un type de compilateur qui prend le code source de programmation et le compile dans un autre langage, exemple babel et webpack.

### <span style="color: #aaffff">**Outil et framework**</span>
Angular et React côté client.<br/>Node.js côté serveur.

### <span style="color: #aaffff">**Où écrire du code JavaScript**</span>

Soit on crée un fichier point js ou dans une balise script dans le fichier html.
```html
<body>Dans le fichier.html
    <script type="test/javascript">
        let name = "Daniel";
        console.log('contenu de la variable name:', name);
    </script>
</body>    

```
```html
<body>appele du fichier.js
    <script src="fichier.js"></script>
</body>    

```
### <span style="color: #aaffff">**Instruction et bloc d'instruction**</span>
Une ligne de code se termine toujours par un point-virgule.<br>Un bloc d'intruction est déliité par des accolades {}.

### <span style="color: #aaffff">**Les variables et constantes**</span>

Les variables sont déclarées par le mot-clè (let) suivi d'un nom en anglais décrivant le contenu de la variable terminé par (;).<br>Elles sont sensible à la casse et on la nomme en camelCase.
```javascript
    let name = "Daniel";
    let age = 25;
    age = 55;//réaffetation de la valeur de age
```
Les constantes sont déclarées avec le mot-clè (const), sa valeur ne peut pas être réaffectée car elle est constante.
```javascript
    const name = "Daniel";
    const surName = "Dan";

```
### <span style="color: #aaffff">**Les types de variables**</span>
number = pour les nombres.<br>string = pour les chaine de caratères.<br>booléen = pour true ou false.<br>objet = pour les objets et tableaux.

typeof, permet d'affiche le type de la variable ou de la constante.
```javascript
    const name = "Daniel";
    let age = 55;
    console.log('affiche le type:', typeof name, typeof age);
```
Affichera dans la console string pour name et number pour age.

### <span style="color: #aaffff">**Le +unaire**</span>
Le +unaire permet de convertir la string en nombre.
```javascript
    let unaire = "12";
    console.log('plus unaire:', +unaire, typeof +unaire);
```
pour incrémenter faire ++.<br>
pour décrémenter faire --.<br>
pour les puissances faire **.
```javascript
    let nombre = 2;
    let puissance = 2**2;
    console.log('puissance:', puissance, typeof puissance);
```
### <span style="color: #aaffff">**Opérateurs de comparaison**</span>
égalité simple, compare seulement la valeur avec  ==<br>
égalité strict, compare la valeur et le type avec ===<br>
l'inégalité avec !==<br>
strictement inférieur avec < <br>
strictement supérieur avec > <br>
inférieur ou égal avec <= <br>
supérieur ou égal avec >= 

### <span style="color: #aaffff">**Opérateurs logique**</span>
le ET logique avec && <br>
le OU logique avec || (altGr 6) <br>
le non logique avec ! exemple: non true, !true
```javascript
    let operat = true;
    console.log('non logique de true:', !true);
```
### <span style="color: #aaffff">**Opérateurs d'affectation**</span>
nb += 3 => nb = nb + 3 <br>
nb -= 5 => nb = nb - 5 <br>
nb *= 2 => nb = nb * 2 <br>
nb /= 3 => nb = nb / 3 <br>
nb %= 4 => nb = nb % 4 Modulo, affiche le reste de la division.

### <span style="color: #aaffff">**String**</span> = Chaine de caratères.

````javascript
    //Trois sorte de syntaxe
    let str ='Mon texte entre simple quote';
    str ="Mon texte entre double quote";
    str =`Mon texte avec les backticks`;
    // échapper un caractère
    str ='C\'est';//avec un back-slash
    // retour à la ligne avec(\n).
    // retour chariot avec (\r).
    //Concaténation avec le signe +
    let str2 ='Ma seconde chaine';
    console.log(str + str2 + "texte 3");
   
````
#### <span style="color: #aaffff">**Où se documenter pour manipuler les strings**</span> => Sur le MDN.

#### <span style="color: #aaffff">**La longueur d'une string**</span> 
Avec la propriété (.length). Les espaces sont considérés comme des caratères.
````javascript
    const str = "Mon texte";// const ou let
    console.log(str.length, "Mon test".length);
    // affichera 9 pour Mon texte et 8 pour Mon test.
    // on peux test sa longueur
    console.log(str.length > 3);
````
#### <span style="color: #aaffff">**Mettre en majuscule**</span> 
Avec la méthode (toUpperCase).
`````javascript
    let str = " texte en minuscule";
    str = str.toUpperCase();// pour réassigner str en majuscule.
    console.log(str.toUpperCase(), "en minuscule".toUpperCase());
`````
#### <span style="color: #aaffff">**Mettre en minuscule**</span>
Avec la méthode (toLowerCase).
`````javascript
    let str = " texte en minuscule";
    str = str.toLowerCase();// pour réassigner str en majuscule.
    console.log(str.toLowerCase(), "en minuscule".toLowerCase());
`````
#### <span style="color: #aaffff">**Accéder à un caractère grâce à sa position**</span>
Accéder à un caractère grâce à sa position en mettant des crochets. Les position en javascript commencent à zéro.
`````javascript
    let str = "Jean";
    
    console.log(str[0], str[3], str[str.length - 1]);
    // str.length - 1, Méthode dynamique pour savoir la position de dernier caractère.
`````
#### <span style="color: #aaffff">**La position d'une string dans une string**</span>
Avec la méthode (indexOf).
`````javascript
    let str = "Bonjour, Jean";
    
    console.log(str.indexOf("Jean"));
    // indiquera que Jean est à la position 9.
    // si ce que je recherche n'est pas présent, indexOf retournera -1.
`````
#### <span style="color: #aaffff">**détermine si une chaîne de caractères est contenue dans une autre**</span>
La méthode <span style="color: #2EFE2E">includes()</span> détermine si une chaîne de caractères est contenue dans une autre et renvoie true ou false selon le cas de figure.
`````javascript
    str = 'Il est allé se balader';
    let search = 'se balader';
    console.log(str.includes(search));
`````
#### <span style="color: #aaffff">**détermine si une chaîne de caractères commence dans une autre**</span>
La méthode <span style="color: #2EFE2E">startsWith()</span> renvoie un booléen indiquant si la chaine de caractères commence par la deuxième chaine de caractères fournie en argument.
`````javascript
    str = 'Bonjour Jean !';
    const start = "Bonjour, !";
    console.log(str.startsWith(start));
`````
#### <span style="color: #aaffff">**Remplace toutes les lettres "a" par "o"**</span>
La méthode <span style="color: #2EFE2E">replaceAll(1,2)</span> remplace le premier argument par le deuxième.
`````javascript
    str = 'Ma banane a planté';
    console.log(str.replaceAll("a","o"));
    //Affichera Mo bonone o plonté
`````
#### <span style="color: #aaffff">**Séparez grâce à un espace (" ") tous les mots contenues dans la variable "str"**</span>
La méthode <span style="color: #2EFE2E">split()</span> permet de diviser une chaîne de caractères à partir d'un séparateur pour fournir un tableau de sous-chaînes.
`````javascript
    str = 'Une phrase complexe avec des mots';
    console.log(str.split(' '));
    //Affichera: Array(6) [ "Une", "phrase", "complexe", "avec", "des", "mots" ]
`````

#### <span style="color: #aaffff">**Supprimer des espaces en début et fin d'une chaine de caractère**</span>
La méthode <span style="color: #2EFE2E">trim()</span> permet de retirer les blancs en début et fin de chaîne. Les blancs considérés sont les caractères d'espacement (espace, tabulation, espace insécable, etc.) ainsi que les caractères de fin de ligne (LF, CR, etc.).
`````javascript
    str = '        Mon text avec trop d\'espace        ';
    console.log(str.trim());
    //Affichera: Mon text avec trop d'espace
``````

### <span style="color: #aaffff">**Array présentation.**</span>
Un Array est un tableau, le premier index commence à zéro.<br>
On peut connaitre la longueur d'un tableau avec .lenght-1.<br>
On les utilisee pour stocker des données.<br>
On trouvera sur le MDN Array la documentation d'un tableau.<br>

### <span style="color: #aaffff">**Créer un tableau.**</span>
On utilise des crochets et à l'intérieur on mets des valeurs séparées par une virgule.<br>
On sauvegarde le tableau dans une constante.<br>
`````javascript
    const arr =[1, 2, 3, 4, 5];
    console.log(arr, arr.lenght);
`````
arr.lenght, affiche la longueur du tableau.<br>
Dans un tableau, on peut y mettre des chaines de caractètes, des nombres, des booléens, d'autres tableaux, la valeur null.
On peut créer un tableau aussi avec new.<br>
````javascript
    let arr2 = new Array(10);
    arr2.fill(false);
    console.log(arr2);
````
Array(10), pour la longueur du tableau ici 10, créera 10 valeur de 10.<br>
.fill(false), permet de remplir le tableau avec 10 fois false.<br>

#### <span style="color: #aaffff">**Accéder à une valeur grâce à son index.**</span>
On accède à une valeur d'un tableau grâce à son index.<br>
````javascript
    const arr3 = ["Sam", "Jean","Pierre"];
    console.log(arr3, arr3[0], [1, 2, 3].length);
````
arr3[0], permet d'accèder à la valeur dont la position est zéro, ici Sam si je mets 3, il affichera Pierre.<br>
[1, 2, 3].length), affichera la longueur du tableau.<br>
[1, 2, 3].length-1), affichera le dernier élément d'un tableau.<br>

#### <span style="color: #aaffff">**Insérer une valeur dans le tableau.**</span>
.push(), insère une valeur dans le tableau, ici 600.<br>
````javascript
    const arr3 = [];
    arr3.push(600);
    console.log(arr3);
````
il est préférable d'avoir le même type de données dans un tableau.<br>
Par exemple, un tableau pour les noms, et un tableau pour les prix, etc....<br>

#### <span style="color: #aaffff">**Supprimer ou remplacer une valeur avec la méthode " .splice() ".**</span>

``````javascript
    const arr = [];
    // insérer des données
    arr.push("Jean", "Sam", "Pierre");
    console.log(arr);
    
    // supprimer des données
    arr.splice(0,1);//supprimera Jean
    console.log(arr);
    
    //supprimer tout les éléments à partir de 
    arr.splice(1, arr.length);//supprimera tout les éléments à partir de Sam, jusqu'à la fin du tableau.
    console.log(arr);

    // remplacer un élément
    arr.splice(1,1, "Samy");// supprimera "Sam" pour le remplacer par "Samy"
    console.log(arr);
``````
start:0, est la position de l'élément à supprimer ici 0, avec -1 on supprime le dernier élément.<br>
deleteCount: 1, est le nombre d'élément à supprimer ici 1.<br>


## <span style="color: #aaffff">**Les conditions :**</span>

#### <span style="color: #ff0000"> if... else :</span>
L'instruction <span style="color: #ff0000"> if</span> exécute une instruction si une condition donnée est vraie ou équivalente à vrai. Si la condition n'est pas vérifiée, il est possible d'utiliser une autre instruction.<br>
````javascript
    let firstname = 'Daniel';

    if (firstname == 'Daniel') { // On passe dans cette condition
    console.log("ma condition est vraie !");
    }

    if (firstname == 'Pierre') { // On ne passe pas dans cette condition
    console.log("ma condition est vraie !");
    }
````
Il est possible de chaîner plusieurs instructions <span style="color: #ff0000"> if</span>, ainsi que de les imbriquer les unes dans les autres.<br>
````javascript
    let age = 24;
    let firstname = 'Tristan';
    let ville = 'Nantes';
    
    if (age == 24) {
        if (firstname == 'Tristan') {
            if (ville == 'Nantes') {
            // Toutes les conditions sont remplies
            }
        }
    }
````
Il est possible de définir un bloc de code à exécuter dans le cas où la condition ne serait pas remplie, via une instruction <span style="color: #ff0000">if...else</span>.<br>
````javascript
    let age = 24;

    if (age == 24) {
        console.log("L'age est bien 24 !");
    } else {
        console.log("L'age est différent de 24");
    }
````
Il est également possible d'utiliser l'instruction <span style="color: #ff0000">else if</span> de manière à couvrir plusieurs cas et à n'exécuter que le bloc de code correspondant à la condition évaluée à vrai. Ce bloc peut s'utiliser plusieurs fois de suite, on parle alors de chaîne.<br>
````javascript
    let age = 24;

    if (age == 20) {
        // âge 20
    } else if (age == 22 ) {
        // âge 22
    } else if (age == 24 ) {
        // âge 24
    } else {
        // L'âge n'est pas 20, 22 ou 24
    }
````
Si une des conditions est évaluée à vrai, le programme n'évaluera pas les conditions suivantes.<br>
````javascript
    let age = 17;

    if (age >= 18) {
        console.log('Vous êtes majeur.');
    } else {
        console.log('Vous êtes mineur.');
    }
````
#### <span style="color: #aaffff">  Le ternaire :</span>
L'opérateur ternaire <span style="color: #ff0000">( ? : )</span> est le seul opérateur à comporter trois opérandes. Une expression ternaire se définit la plupart du temps sur une seule ligne, et vérifie si une condition est vraie ou fausse.<br>
``````javascript
    let x = 0;
    let y = 9;
    let isEqual;

    // Cette condition...
    if (x == y) {
        isEqual = true
    } else {
        isEqual = false
    }

    // ... est équivalente à cette condition ternaire
    x == y ? isEqual = true : isEqual = false
``````
````javascript
    let lastname = 'Brassens';
    let genre = 'femme';

    console.log((genre == 'femme' ? 'Mme ' : 'M ') + lastname);
````
#### <span style="color: #aaffff"> Switch:</span>
L'instruction <span style="color: #ff0000">switch</span> permet d'évaluer une expression donnée et d'exécuter les instructions correspondant au cas qui y est associé.<br>
````javascript
    switch (condition) {
    case valeur1:
        // Instructions à exécuter lorsque le résultat de la condition correspond à valeur1
        instructions1;
        break;
    case valeur2:
        // Instructions à exécuter lorsque le résultat de la condition correspond à valeur2
        instructions2;
        break;
    default:
        // Instructions à exécuter lorsqu'aucune des valeurs ne correspond à la condition à évaluer
        instructions-par-défaut;
        break;
};

````
````javascript
    let fruit = "kiwi";

switch (fruit) {
    case "banane":
        console.log("Vous avez choisi une banane.");
        break;
    case "pomme":
        console.log("Vous avez choisi une pomme.");
        break;
    case "kiwi":
        console.log("Vous avez choisi un kiwi.");
        break;
    case "clémentine":
        console.log("Vous avez choisi une clémentine.");
        break;
    default:
        console.log("Vous n'avez pas choisi un fruit de la liste.");
        break;
};

// Affichera "Vous avez choisi un kiwi."
````
#### <span style="color: #aaffff"> Les boucles : La boucle for :</span>
L'instruction <span style="color: #ff0000">for</span> va nous permettre d'exécuter en boucle (le nombre de fois défini) un même bloc d'instructions.

Le premier paramètre de l'instruction <span style="color: #ff0000">for</span> est la valeur d'initialisation (comme un compteur).

Le second est une condition évaluée avant chaque passage dans la boucle, si la condition est vérifiée, alors l'instruction est exécutée.

Le troisième paramètre est une expression évaluée à la fin de chaque itération (passage dans la boucle).<br>
````javascript
    // let i=0,déclaration et initialisation de i,i < 6 est la condition, i++ est l'incrémentation
    for (let i = 0; i < 6; i++) {
        console.log(i)
};
````
#### <span style="color: #aaffff"> Les boucles : L'instruction do... while :</span>
<span style="color: #ff0000">do...while :</span> la différence avec while est que la condition est analysée après l'exécution du bloc d’instructions.<br>
````javascript
    let result = '';
    let i = 0;

    do {
        i = i + 1;
        result = result + i;
    } while (i < 5);

    console.log(result);
````
#### <span style="color: #aaffff"> Les boucles : L'instruction while :</span>
<span style="color: #ff0000">While</span> va permettre de créer une boucle qui va s'exécuter tant que la condition renseignée entre parenthèses est vérifiée.<br>
```javascript
    let n = 0;

    while (n < 3) {
        n++;
    };

    console.log(n);
```

### <span style="color: #aaffff">Les fonctions : Déclarer une fonction.</span>
Déclarer une fonction avec le mot clé (function), lui donner un nom, ouvrir des parenthèse et indiquer les arguments de la fonction (nb, nb2), ouvrir des accolades et y mettre les instruction de la fonction.
````javascript
    function multiplyBy (nb = 30, arguments) {
    console.log('nb:', nb);
    console.log('nb2:', arguments);
    
    return nb * nb2;
};
````
return, permet de retourner une donnée.<br>
On peut mettre une valeur par défaut à un argument, en faisant (nb = 30).<br>
Une fonction peut avoir plusieurs arguments ou aucun.<br>
Un argument peut avoir une valeur par défaut (nb = 5, nb2)

On peut utiliser le mot clé return, pour retourner le résultat de la fonction.<br>

#### <span style="color: #aaffff"> Appeler la fonction.</span><br>
Ecrire le nom de la fonction et entre les parenthèses mettre les paramètres de la fonction.
````javascript
    let produit = multiplyBy (2, 4);
    console.log('produit;', produit);
````
#### <span style="color: #aaffff"> Déclaration d'une fonction qui affiche les valeurs d'un tableau.</span>
````javascript
    let arr = [2, 4, 5, 14, 54, 65, 25, 32];
    console.log('tableau:', arr);
    function afficheChaquevaleur(arr) {
        for (let i=0; i < arr.length; i++) {
            console.log(arr[i]);
        }
    };

    afficheChaquevaleur(arr);
    afficheChaquevaleur(['Jean', 'Sam', 'Jo']);
````
````javascript
    let arr = [2, 4, 5, 14, 54, 65, 25, 32];

     function afficheChaquevaleur(arr) {
        let newarr =[];
        for (let i=0; i < arr.length; i++) {
            newarr.push(arr[i] * 2);// ajouter au tableau (push).
        }
        return newarr;
    };
    console.log(afficheChaquevaleur(arr));
````
### <span style="color: #aaffff">  Les fonctions : Fonction anonyme:</span>
C'est une fonction qui n'a pas de nom.
````javascript
    let fn = function (){
        return 1;
};

    fn(); // appel de la fonction anonyme fn.
    console.log(fn()); // retournera, affichera toujours 1.
````
### <span style="color: #aaffff">  Les fonctions : Fonction qui s'appelle elle même :</span><br>
Elle est mise entre parenthèse.
````javascript
    (function (nb) {
        console.log('elle même :', nb);
    })(2);

````

### <span style="color: #aaffff">  Les fonctions : Fonction callback :</span>
C'est une fonction placé en argument d'une autre fonction.<br>
````javascript
    function maCallback(nb) {
        return nb * 3;
}

    function maFonction(nb, callback) {
    
        let var1 = nb *16;
        
        return callback(var1);
    }
    
   let res =  maFonction(6, maCallback); // maCallback sans parenthèse, on fait référence à cette fontion.

    function maCallback2(nb) {
        return maCallback(nb * 5);
    }

    console.log(res, maFonction(5, maCallback2));
````
### <span style="color: #aaffff">  Les fonctions : l'objet arguments:</span>

````javascript
    function maFn() {
        console.log(arguments);
}
    maFn(1, 3, 4, 5, 6, 7, "Jean", true, null);
````
On peut aussi accéder à une valeur dans arguments avec son index (arguments[0];).<br>
On peut test arguments (if(arguments[5] === "Sam){
    alert('Vous êtes Sam');
    }) à finir à 3min  
### <span style="color: #aaffff">  Les objets : Déclarer un objet:</span>
Un objet c'est la représentation de données sous forme (clè:valeur).<br>
Chaque caractéristique lié à un objet sera appelé une propriété.<br>
Toutes les actions lié à un objet s'appelleront des méthodes.<br>
````javascript
   //Déclaration d'un objet.
    let phone = {
        // propriétés
        name: 'Iphone',
        marque:'apple',
        largeur: 5,
        longueur: 6,
        
        // méthodes
        call: function () {
            console.log('Call Sam');
        },
        sendMsg: function (to) {
            console.log('Send sms to' + to);
        }
        
    };

    console.log(phone);
    
    // pour accéder à un propriété d'un objet.
    console.log('marque:', phone.marque);
    
    // accéder à une méthode dans un objet.
    phone.call();
    phone.sendMsg('Sam');
    
    // rajouter une valeur dans un objet.
    phone.color = red;
    console.log(phone.color, phone["marque"]);// on peut également accéder aux propriété avec les crochets, on donne la clé ["marque"].

    // On peut aussi y accéder avec une variable.
    let nomPropriété = "largeur";
    console.log(phone[nomPropriété]);

````
### <span style="color: #aaffff">   Les objets : Accéder à l’objet courant dans une méthode:</span>
````javascript
    let student = {
        name: 'Sam',
        class:'Terminal',
        say: function (say) {
            // nom de l'étudiant qui dit bonjour.
            console.log(this.name + ': ' + say); //accés à name avec (this) qui fait référence à l'objet courant ici student.
        }
};
    let student2 = {
        name: 'Jean',
        class:'Seconde',
        say: function (say) {
            // nom de l'étudiant qui dit bonjour.
            console.log(this.name + ': ' + say); //accés à name avec (this) qui fait référence à l'objet courant ici student.
        },
        // méthode saveClass pour accéder ou modifié mon objet, qui prend en paramètre ClassName et qui enregistre la class dans mon oblet courant.
        saveClass: function (className) {
            this.class = className;// accés à class avec (this) de l'objet courant.
        }
};
    student2.say('Bonjour' + student.name);
    student.say('Bonjour');
    
    student2.saveClass('6ième');// va enregistrer dans this dans l'objet courant la propriété class avec se que j'ai donné en argument ici '6ième'.
    console.log('Classe de student2:', student2.class);
    
    
````
### <span style="color: #aaffff">Spread operator</span>
Le spread operateur se sont les trois petit point(...) qui veut dire étaler.<br>
Utilisé très souvent.
````javascript
    // Spread Operator
    let arr1 = ["Jean", "Sam", "Tom"];
    let arr2 = ["Pauline", "Lise", "Eloïse"];
    
    let arr3 = [...arr1, ...arr2]//permet d'étaler, arr3 contient les valeurs du tableau arr1 et arr2.
    
    console.log(arr3);
````
ils peuvent être utilisé aussi dans des objets.<br>
````javascript
    // Spread Operator
    let studentDefault = {
        name: "Jean",
        class: null,
    };
    let student = {
        ...studentDefault,
        name: "Sam", // écris Sam dans name, permet de modifié une valeur.
        presence: true // rajoute une propriété.
    };
    console.log(student.name);
````
il ne faut jamais copié un tableau directement, sinon se sera par référence et les valeurs du tableau original seront modifiés.<br>
A la place , on utilise le Spread Operator pour copié les valeurs du tableau d'origine, ou de l'objet que l'on veux copié.<br>
````javascript
    // Spread Operator sur un objet.
    let studentDefault = {
        name: "Jean",
        class: null,
    };
    let student = {
        ...studentDefault,
        name: "Sam", // écris Sam dans name, permet de modifié une valeur.
        presence: true // rajoute une propriété.
    };
    let student2 = {...student};// je fais une copie de student.
    
    student2.name = 'lise';// ensuite je peut modifié le name dans student2, cela n'affectera pas la valeur dans student.
    console.log(student.name);
````

````javascript
    // Spread Operator sur un tableau.
    let arr1 = [1, 3];
    let arr2 = arr1; // ici on copié les données par référence(danger).
    arr2.push(5);// affectera(modifiera) arr1 et arr2.
    
    let arr2 = [...arr1];// créera une copie de arr1.(conseillé).
    arr2.push(5);// modifiera arr2 mais pas arr1.

    console.log(arr1, arr2);
    
    // on peut l'utilisé plusieurs fois.
    let arr =[1];
    let arr1 =[2];
    let arr2 =[3];
    let arr3 =[4];
    
    let arr4 = [...arr, ...arr1, ...arr2, ...arr3];
    console.log(arr4);// affichera un tableau avec les valeurs des autres tableaux.

    // même chose pour les objets.
    let obj = {a: 1};
    let obj1 = {a: 2};
    let obj2 = {...obj, ...obj1};
````
### <span style="color: #aaffff"> Affecter par décomposition</span>
````javascript
    //Affecter par décomposition

    let a, b;// déclaration des variables
    let arr = [1, "J"];
    // Assignation de arr aux variables a et b.
    [a, b] = arr;// on affecte a et b en décomposant le tableau arr.
    console.log(a);// a, aura 1
    console.log(b);// b, aura "j"

    // si mon tableau a plus de données.
    let a, b, rest;// déclaration des variables
    let arr = [1, "J", true, 'Bonjour'];
    // Assignation de arr aux variables a, b et rest.
    [a, b, ...rest] = arr;// ...rest, étalera les données à la suite.
    console.log(a);// a, aura 1, première valeur du tableau
    console.log(b);// b, aura "j"deuxième valeur du tableau.
    console.log(rest);// rest affichera, true et 'Bonjour'.

    // Affectation avec déclaration.
    const [a, b, ...rest] = [1, 2, 3, 4, 5];
    console.log(a);// a, aura 1
    console.log(b);// b, aura 2
    console.log(rest);// rest affichera, 3, 4, 5.

    // Affecter à partir d'un autres objet.
    let name;    
    ({name} = {name: "Jean"});// mettre entre parenthèse, décomposition par le nom de la clé (name:).
    console.log('name :', name);
    
    // ou aussi
    let name, age;
    let student = {name:'Jean',class: "B", age: 20};
    ({name, age} = student);// ici il n'y a pas d'ordre, on récupère les clés que l'on demande (name et age).
    
    console.log('name :', name, age);

    // ou aussi    
    let student = {name:'Jean',class: "B", age: 20};
    let {name, age} = student;// ici pas besoin des parenthèses, car on la déclare directement une variable name et age en décomposants l'objet student.
    name = 'Tom';
    console.log('name :', name, age, student);
    
    // renommer une propriété 
    let student = {name:'Jean',class: "B", age: 20};
    let {name: nom, age} = student;// name: nom, pour renommer la clé name par nom.

    let student = {name:'Jean',class: "B"};
    let {name: nom, age = 20} = student;
    // age = 20,mettra 20 par défault dans age si age n'est pas définit dans student.
    
    console.log('name :', nom, age, student);
    
````
### <span style="color: #aaffff">Manipuler les tableaux</span>
Méthodes à connaître par coeur:<br>
- forEach()<br>
- find()<br>
- findIndex()<br>
- map()<br>
- reduce()<br>
- filter()<br>
- some()<br>
- every()<br>

### <span style="color: #aaffff">Manipuler les tableaux : La méthode "forEach"</span>
La méthode forEach, permet d'exécuter une fonction sur chaque élément du tableau.
````javascript
    // Méthode forEach().
    let arr = [1, 4, 6, 8];
    arr.forEach(function (value,index) {  // forEach, pour chaque.        
        console.log(index,value);
    });
````

### <span style="color: #aaffff">Manipuler les tableaux : La méthode " find"</span>
La méthode find(), retourne la première valeur trouvée répondant à une condition donnée, passée en paramètre sous la forme d'une fonction.Si aucune valeur ne respecte la condition, alors la méthode renvoie : undefined.Cette méthode ne parcourt donc pas forcément le tableau en entier, ce qui peut représenter un gain de performance !<br>
Elle permet de trouver une valeur.
````javascript
    let arr = ["Jean", "Sam", "Steve", "Tom"];

    let callback = function (name, index) {
        if (name.includes('o')) {
            return true;
        } else {
            return false;
        }
    };
    let res = arr.find(callback);
    console.log('res :', res);
````
````javascript
    // ou aussi en plus court.
    let arr = ["Jean", "Sam", "Steve", "Tom"];

    let callback = function (name) {        
            return name.includes('o');        
    };
    let res = arr.find(callback);
    console.log('res :', res);
````
````javascript
    // ou encore comme ceci.
    let arr = ["Jean", "Sam", "Steve", "Tom"];

    let res = arr.find(function (name) {
        return name.includes('o');
    });
    console.log('res :', res);
````
- find() : retourne la première valeur trouvée répondant à une condition donnée.

### <span style="color: #aaffff">Manipuler les tableaux : La méthode "findIndex"</span>
- findIndex() : retourne l'index de la première valeur trouvée répondant à une condition donnée.<br>
````javascript
    let arr = ["Jean", "Sam", "Steve", "Tom"];
    
    let callback = function (element, index) {
        console.log(element,index); // vérifier avec quoi on travail, ici element et index.
        return element.length === 5;    
    };
    let index = arr.findIndex(callback);
    
    console.log('Index :', index, arr[index]);// (index)affichera l'index et arr[index], affichera la valeur dans l'index.
````

### <span style="color: #aaffff">Manipuler les tableaux : La méthode "map"</span>
- map() : permet de créer un tableau à partir d'un autre tableau, souvent utilisé pour modifier un tableau ou pour recréer un tableau à partir d'un autre tableau.<br>
````javascript
    // pour les nombres.
    let arr = [1, 2, 4, 56, 78, 99, 123, 456];

    let callback = function (value, index) {
        console.log(value, index);
        return value % 2 ? 'Impair :'+ value : 'Pair :' + value ;
    }
    
    let arr2 = arr.map(callback);
    
    console.log(arr2);

    // pour modifier un tableau qui contient des objets.
    let arr = [
        {age:22 },
        {age:17 },
        {age:56 },
    ];
    
    let callback = function (value, index) {
        console.log(value);
        return value.age;
    }
    
    let arr2 = arr.map(callback);
    
    console.log(arr2);
  
````

### <span style="color: #aaffff">Manipuler les tableaux : La méthode "reduce"</span>
- reduce() : permet de réduire un tableau sous une donnée, ou un autre type de données.<br>
````javascript
    let arr = [3, 4, 49, 230, 48, 123];
    
    let accumulation = arr.reduce(function (acc,value, index) { 
        console.log(acc, value, index);// toujours vérifier avec quoi on travail.
        
        acc = acc + value;// acc, valeur d'accumulation.
        
        return acc;
    }, 0);// zéro est la valeur initial de (acc) que l'on a définit.

    console.log(accumulation, arr);
````

### <span style="color: #aaffff">Manipuler les tableaux : La méthode "filter"</span>
- filter() :permet de filtrer des éléments d'un tableau en fonction d'une condition.
````javascript
    let arr = [3, 4, 49, 230, 7, 48, 123];
    
    let arrfiltered = arr.filter(function (nb, index) { 
        console.log(nb, index);
        if (nb % 2) {
            return true;
        } else {
            return false;
        }
    });
    
    console.log('array filtré :', arrfiltered);
    
    // simplifié
    let arr = [3, 4, 49, 230, 7, 48, 123];
    
    let arrfiltered = arr.filter(function (nb, index) {
        console.log(nb, index);
        return nb % 2;// si le nombre est impair, ça retournera 1, et 1 = true.
        // return !(nb % 2); si le nombre est pair, ça retournera 0, et 0 = false.
    });
    
    console.log('array filtré :', arrfiltered);
````

### <span style="color: #aaffff">Manipuler les tableaux : La méthode "some"</span>
- some() : teste si au moins un élément du tableau passe le test implémenté par la fonction fournie. Elle renvoie un booléen indiquant le résultat du test.<br>
````javascript
    let arr = [3, 4, 49, 230, 7, 48, 123];

   let cond =  arr.some(function (value, index) { 
        console.log(value, index);// je vérifie avec quoi je travail.
        
        if (value < 10) {// condition value inférieur à 10
            return true;
        } else {
            return false;
        }
    });
   
    console.log(cond);
````
````javascript
    // simplifié
    let arr = [3, 4, 49, 230, 7, 48, 123];

    let cond = arr.some(function (value, index) { 
        console.log(value, index);// je vérifie avec quoi je travail.
        
        return value < 10;// condition value inférieur à 10
    });
    
    console.log(cond);
````

### <span style="color: #aaffff">Manipuler les tableaux : La méthode "every"</span>
- every() : permet de tester si tous les éléments d'un tableau vérifient une condition donnée par une fonction en argument. Cette méthode renvoie un booléen pour le résultat du test.<br>
Permet de test un tableau et de vérifier que toutes les valeurs du tableau valide une condition.
````javascript
    let arr = [3, 4, 49, 230, 7, 48, 123];

    let cond = arr.every(function (value, index) {
        console.log(value, index);// je vérifie avec quoi je travail.
        
        return value > 0;
    });

    console.log('Toute les valeurs :', cond);
````

### <span style="color: #aaffff">51. Les classes : Déclarer une classe</span>
Une classe c'est un type de fonction, qui permet de créer des objets.<br>
On utilise le mot clé class, puis le nom de la classe avec une majuscule au début et sur chaque nouveau mots qui suit.Après on ouvre des accolades
````javascript
    // déclaration de la class
    class User {
    
    }   
    // création d'une instance de la class User avec new User().
    let user = new User();

    // Ajouter une propriété avec sa valeur dans une instance.
    user.name = "Jean";

    // afficher l'instance de la variable user
    console.log(user);
````
Une instance et un objet crée à partir de la class.<br>
Les classes sont des modèles d'objets.<br>

###<span style="color: #aaffff">52. Les classes : Propriété par défaut</span>
```javascript
    // déclaration de la class(modèle).
    class User {
        name = "Jean";// propriété de la class User.
        age = 22;
        presence;// pas de valeur, affichera undefined
    }   
    // création d'une instance de la class User avec new User().
    let user = new User();

    // afficher l'instance de la variable user
    console.log(user);
````
### <span style="color: #aaffff">53. Les classes : La fonction "constructor()"</span>
La méthode constructor(), elle sert à créer le nouvel objet.C'est un mot réserve à javascript.
```javascript
    // déclaration de la class(modèle).
    class User {
        name;// propriété de la class User.
        age;
        presence;
        pays = "France";// valeur par défaut.
        
        constructor(name, age, presence = false) {
            this.name = name;// propriété dynamique.
            this.age = age;
            this.presence = presence;
        }
        
    }   
    // création d'une instance de la class User avec new User().
    let user = new User("Tom", 27, true);
    let user2 = new User("Sam", 34);

    // afficher l'instance de la variable user
    console.log(user);
    console.log(user2);
    
````
### <span style="color: #aaffff">54. Les classes : Ajouter des méthodes</span>
```javascript
    // Modèle
    class User {
        name;// propriété de la class User.
        age;
        presence;
        pays = "France";// valeur par défaut.
        
        constructor(name, age, presence = false) {
            this.name = name;// propriété dynamique.
            this.age = age;
            this.presence = presence;
        }
        // déclaration d'une méthode
        isPresent() {
            return this.presence;
        }
        
        // déclaration seconde méthode.
        say(str) {
            console.log(this.name + ' : ' + str);
        }
        
    }   
    // création d'une instance de la class User avec new User(), ce sont ddes objets.
    let user = new User("Tom", 27, true);
    let user2 = new User("Sam", 34);

    // pour accéder à la méthode.
    // user.isPresent();
    //user2.isPresent();
    
    // afficher l'instance de la variable user
    console.log(user, user.isPresent());
    console.log(user2, user.isPresent());
    
    user.say("Bonjour " + user2.name);
    user2.say("Bonjour " + user.name);
    
    
````
### <span style="color: #aaffff">55. Les classes : Héritage entre les classes</span>
````javascript
    // modèle
    class Vehicle {
        constructor(name, wheels) {
            this.name = name;
            this.wheels = wheels;// Nbre de roues.
        }
        
        accelerate() {
            
        }
        
        brake() {
            
        }
        
        turn() {
            
        }
        
    }
    // extends, pour hérité de la class Vehicle.
    class Car extends Vehicle { 
        // appel du constructor de la class parent (Vehicle) avec le mot clé (super) .
        constructor(name, coffre) {
            super(name, 4);
            this.coffre = coffre;
        }
    }
    
    class MotorBike extends Vehicle {
        constructor(model, hasPassenger = false) {
            super(model, 2)
            this.hasPassenger = hasPassenger;
        }
    }
    
    let car = new Car("Audi", 500);
    let car2 = new Car("Mercedes", 200);
    let moto = new MotorBike('honda', true);
    
    console.log(car);
    console.log(car2);
    console.log(moto);
    
    car.accelerate();
    moto.accelerate();
    car.brake();
    moto.turn();
    
    let bike = new Vehicle("Vélo, 2");
    console.log(bike);
````
### <span style="color: #aaffff">56. Les classes : Propriétés et méthodes "static"</span>
Le mot-clé <span style="color: yellow">static</span> permet de définir une méthode statique d'une classe. Les méthodes statiques ne sont pas disponibles sur les instances d'une classe mais sont appelées sur la classe elle-même. Les méthodes statiques sont généralement des fonctions utilitaires (qui peuvent permettre de créer ou de cloner des objets par exemple).<br>
Les méthodes statiques sont utilisées lorsque la méthode ne s'applique qu'à la classe elle-même et pas à ses instances. Les méthodes statiques sont généralement utilisées pour créer des fonctions utilitaires.<br>
````javascript
    class ClassWithStaticMethod {
    
      static staticProperty = 'someValue';
      static staticMethod() {
        return 'static method has been called.';
      }
    
    }
    
    console.log(ClassWithStaticMethod.staticProperty);
    // affichera en output: "someValue"
    console.log(ClassWithStaticMethod.staticMethod());
    // affichera en output: "static method has been called."

````
![staticJavaScript.png](staticJavaScript.png)

### <span style="color: #aaffff">57. Manipuler le dom : S'assurer que le dom est chargé "static"</span>
````javascript
    // première moyen de savoir si le Dom est chargé avant de le manipuler.
    window.onload = function () {
        console.log('4');
        console.log(document.querySelector('h1'));
    };

    // deuxième façon
    window.addEventListener('DOMContentLoaded', function () {
        console.log('5');
        console.log(document.querySelector('h1'));
    })
````
### <span style="color: #aaffff">58. Manipuler le dom : Sélectionner un élément grâce à son id</span>
````javascript
    //cibler un id
    const square = document.getElementById('sqr');
    console.log(square);// affichera: <div id = "sqr" ></div>
````
les (id) ont les utilise principalement pour manipuler les éléments avec du javascript.
Cette méthode renvoie l'élément du DOM qui possède l'id = 'sqr'.
```javascript
    //cibler une class
    const squares = document.getElementsByClassName('square');
    console.log(squares);
````
Cette méthode renvoie la liste des éléments du DOM ayant pour attribut class="square".<br>

### <span style="color: #aaffff">59. Manipuler le dom : Sélectionner des éléments grâce à leurs classes</span>
````javascript
    // 
    const square = document.querySelector('.square');// nommé la class avec un point.
    
    console.log(square);
    
````
### <span style="color: #aaffff">60. Manipuler le dom : Sélectionner un ou des éléments</span>
````javascript
    // 
    const square = document.querySelector('.square.bigSquare');// pour plusieurs class, les mettre à la suite.
    const square = document.querySelector('#sqr');// pour l'id
    const square = document.querySelector('section');// avec le nom du tag (balise).
    const square = document.querySelectorAll('section');// rechercher plusieurs éléments avec le nom du tag (balise section).
    console.log(square);
    
````
### <span style="color: #aaffff">61. Manipuler le dom : Manipuler le style d'un élément</span>
````javascript
    // modifié un élément
    // 1er je le sélectionne.
    const square = document.querySelector('.square');
    // je m'assure qu'il est bien sélectionné.
    console.log(square);
    // je modifié son style.
    square.style.backgroundColor = 'yellow';
    square.style.border = '1px solid grey';
    // modifié sa position.(fixée à 200px du top(haut).
    square.style.position = 'fixed';
    square.style.top = "10px";
    console.log(square);
    
    
    // avec un interval de temps.
    let positionY = 0;// vertical
    
    setInterval(function () {
        positionY++;
        square.style.top = positionY + "px";
    }, 2000 );// 2000 ms égal 2 secondes.

    let positionX = 0;// horizontal
    
    setInterval(function () {
        positionX++;// ou, position = position + 10;
        square.style.left = positionX + "px";
    }, 100 );// 100 ms égal 0.1 secondes.
    console.log(square);
    
    // changer la couleur après un temps donné.
    let i = 0;
    setInterval(function () {
         square.style.backgroundColor = i % 2 ? 'red' : 'yellow';
         i++;
    }, 1000 );// 1000 ms égal 1 secondes.
    
````
### <span style="color: #aaffff">62. Manipuler le dom : Manipuler les classes d'un élément</span>
````javascript
    // récupérer l'élément.
    const square = document.querySelector('.square');
    console.log(square);
    
    let i = 0;
    setInterval(function () {
        if (i % 2) {
            square.classList.add('bigSquare');// ajoute la class (bigSquare).
        } else {
            square.classList.remove('bigSquare');// supprime la class (bigSquare).
        }
        i++;
    }, 1000);
````
### <span style="color: #aaffff">63. Manipuler le dom : Créer et ajouter au dom un élément</span>
````javascript
    // créer un élément
    const square = document.createElement('section');

    // ajouter une class square.
    square.classList.add('square');
    
    // ajouter l'élément dans notre page.
    document.body.appendChild(square);
    
    // créer un nouvel élément.
    let container = document.createElement('div');
    
    // ajouter l'élément dans mon body.
    container.innerText = 'container';// ajout d'un texte.
    document.body.appendChild(container);
    
    // ajouter un enfant à container.
    container.appendChild(square);
    
    console.log(square);
````
Ajouter du texte ou une balise html :<br>
````javascript
    // ajouter du texte à notre élément.
    square.innerText = 'ok';
    // insérer du html dans un élément, il serra considéré comme un enfant de votre élément.
    square.innerHTML = '<strong>Ok</strong>';
    
````
### <span style="color: #aaffff">64. Manipuler le dom : Écouter les évènements du dom</span>
````javascript
    // sélectionné le bouton crée dans la page html.
    const btnEl = document.getElementById('createSquare');
    console.log(btnEl);
    
    const redSquareContainerEl = document.getElementById('redSquare');
    
    // ajouter une écoute d'événemment.
    btnEl.addEventListener('click', function () {
        console.log('click', this);// fait référence à lélément qui à été cliqué.
                
        redSquareContainerEl.appendChild(createSquare());
    });
    // créer une fonction qui crée un élément (div).
    function createSquare() {
        const el = document.createElement('div');
        el.classList.add('square');
        return el;
    }
````

````javascript
    // écouter l'événement de la souris.
    window.addEventListener('mousemove', function (event) {
    console.log(event);
});
````

### <span style="color: #aaffff">65. Javascript - Function arrow</span>
Déclarer une fonction fléchée.<br>
````javascript
    // fonction fléchée
function maFonction(nb) {
    console.log(this);// this fait référence à l'objet dans laquelle la fonction est éxécutée, ici window.
    return nb;
}

// Déclaration de la fonction fléchée.

let maFn = (nb) => {
    console.log(this);
    return nb;
};

// appel des fonctions.
console.log('MaFonction :', maFonction(1)) ;
console.log('maFn :', maFn(2)); // raccourci, maFn(2).log, puis tab.Donnera console.log(maFn(2));


let user = {
    name: 'Jean',
    notes: [12, 14, 15],

    // méthode.
    getNotes: function () {
        // context parent
        console.log(this);// this fait référence à l'objet dans laquelle la fonction est éxécutée, ici user, this parent de la fonction fléchée en dessous..
        //this.notes.forEach(function (note) {
        //    console.log(this);// fait référence à la fenêtre (window)
        //    console.log(this.name + ' a eu ' + note);// je n'ai pas accès à la propriété name, pour avoir accès à l'objet parent, il faut faire une fonction fléchée.


        this.notes.forEach((note) => {// grâce à la fonction fléchée, on peut accéder aux propriété de la fonction callback.
            console.log('forEach :', this);// fait référence à la fenêtre (window)
            //context enfant
            console.log(this.name + ' a eu ' + note);// la fonction fléchée va lié le this du contexte parent au this de la fonction.
        });

        /* Ne fonctionne pas sur toutes les méthodes, le plus simple est d'utiliser une fonction fléchée.
        // en mettant un deuxième argument d'une fonction callback.
        this.notes.forEach(function (note)  {
            console.log('For :', this);
            //context enfant
            console.log(this.name + ' a eu ' + note);
        }, this);// en passant le this en deuxième argument, j'ai accès à la propriété name.



        this.notes.forEach(function (note)  {
            console.log('For :', this);
            //context enfant
            console.log(this.name + ' a eu ' + note);
        }, {name: "OK"});// en passant OK via la propriété name, name sera égal à OK.*/

// éxécution de la méthode getNotes à travers l'objet (user).
user.getNotes();
},
};
````


