
## <div style="color: #26B260">**V. Flexbox**</div>
[Retour au sommaire](sommaire.md)


### <span style="color: #aaffff">**flexbox**</span>

Permet de positionner des boites, pour l'utiliser il faut mettre <span style="color: lightblue">*display*</span> à <span style="color: lightgreen">*flex;*</span> dans le css.  
Le comportement de <span style="color: lightgreen">*flex*</span> est d'afficher tous ses enfants sont côte à côte sur un même ligne.

```css
.container {
        display: flex;
}
```
### <span style="color: #aaffff">**1. flex-direction**</span>

Définit la façon dont les éléments sont placés.

<span style="color: lightgreen">*row*</span>, ligne de gauche à droite.  
<span style="color: lightgreen">*row reverse*</span>, ligne de droite à gauche.  
<span style="color: lightgreen">*column*</span>, colonne du haut vers le bas.  
<span style="color: lightgreen">*column-reverse*</span>, colonne du bas vers le haut.  
Par défaut c'est row.
```css
.container {
    flex-direction: row;
}
```

### <span style="color: #aaffff">**2. flex-wrap**</span>
Définit

### <span style="color: #aaffff">**3. justify-content**</span>
définit la place des éléments sur l'axe principal.<br>
<span style="color: lightgreen">*flex-start*</span>, les éléments sont placés au début.  
<span style="color: lightgreen">*flex-end*</span>, les éléments sont placés à la fin.  
<span style="color: lightgreen">*center*</span>,  les éléments sont center.  
<span style="color: lightgreen">*space-between*</span>, les éléments ont un espace entre eux, sauf au début et à la fin.  
<span style="color: lightgreen">*space-around*</span>,  les éléments ont un espace entre eux, au début et à la fin aussi.    
```css
.container {
    justify-content: flex-start;
}
```

### <span style="color: #aaffff">**4. align-items**</span>
définit la place des éléments sur l'axe secondaire.<br>
<span style="color: lightgreen">*stretch*</span>, les éléments sont étirés pour prendre toute la place du conteneur.  
<span style="color: lightgreen">*flex-start*</span>, les éléments sont placés au début del'axe secondaire.  
<span style="color: lightgreen">*flex-end*</span>,  les éléments sont placés à la  de l'axe secondaire.  
<span style="color: lightgreen">*center*</span>, les éléments sont aligné au centre de l'axe secondaire.
<span style="color: lightgreen">*baseline*</span>,  les éléments sont aligné en fonction de leurs ligne de base.
```css
.container {
    align-items: flex-start;
}
```

### <span style="color: #aaffff">**5. align-content**</span>
Définit l'espaceentre et autour des éléménts, les valeurs possible sont les mêmes que justify-content.  
<span style="color: lightgreen">*flex-start*</span>, les éléments sont placés au début.  
<span style="color: lightgreen">*flex-end*</span>, les éléments sont placés à la fin.  
<span style="color: lightgreen">*center*</span>,  les éléments sont center.  
<span style="color: lightgreen">*space-between*</span>, les éléments ont un espace entre eux, sauf au début et à la fin.  
<span style="color: lightgreen">*space-around*</span>,  les éléments ont un espace entre eux, au début et à la fin aussi.
```css
.container {
    align-content: flex-start;
}
```

### <span style="color: #aaffff">**6. order**</span>
Permet de modifier l'order des items (des éléments), permet de définir l'ordre d'affichage.
```css
.one {
    order: 2;
}
```