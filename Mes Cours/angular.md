##  <div style="color: #26B260">**VIIII. Angular**</div>
[Retour au sommaire](sommaire.md)

### <span style="color: #aaffff">**1. arrow function | le mot clé this | opérateur new**</span>
Rappel:

1) déclarer une fonction.

````javascript
function maFonction() {
   // code de la fonction
}
````
2) Méthode, c'est une fonction dans un objet.

Déclaration d'un objet.

````javascript
const video = {
    title: 'titre', // propriété title
    //Méthode play qui affiche this, this fait réference a mon objet (myObjet)
    play() {
        console.log(this);
    }
};
// appel de la méthode play dans l'objet video
video.play();


````
Le this fait référence a mon objet (video)<br>

![img_4.png](img_4.png)

Aperçu dans la console<br>

![img_5.png](img_5.png)


````javascript
const video = {
    title: 'titre', // propriété title
    //Méthode play qui affiche this, this fait réference a mon objet (myObjet)
    play() {
        console.log('1',this);
    }
};
// appel de la méthode play dans l'objet video
video.play();
// ici this fait réference à la fenêtre windows car il n'est pas dans l'objet video.
function maFonction() {
    console.log('1.1',this);
}
// atention car this ne fait pas réference au même endroit.

````
Aperçu dans la console<br>
![img_6.png](img_6.png)

3) Déclaration d'une fonction constructor.
On la nomme en PascalCase.
   
````javascript
// Constructor function
function Video(title) {
    this.title = title; // j'enregistre title dans this.title
    console.log('2',this); // pour voir a quoi fait réference this
}
// Création d' un objet video1 à partir de la fonction Video(title) et va lié le this a cet objet
const video1 = new Video('a');
````
C'est un objet crée à partir de Video.<br>
![img_7.png](img_7.png)

Un constructor function, c'est une fonction qui va créer un objet.

4) factory function.<br>
elle fait la même chose, c'est une usine qui crée des objet
````javascript
// factory function
function createVideo(title) {
    console.log('3',this);
    return {
        title: title,
    };
}
// Création de creatVideo mais sans le mot clé new
const video2 = cretaVideo('3');
console.log('video2', video2);
````
le this du console.log('3',this) fait référence à la fenêtre Window et non pas a l'objet<br>

![img_8.png](img_8.png)

alors que j'ai bien créer un objet.<br>

![img_10.png](img_10.png)

this ne fait pas référence a cet objet mais a l'objet global.

5) fonction callback.
````javascript
const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);// this fait réference à l'objet video        
        this.tags.forEach(function (tag) { // function(tag) est une fonction callback.
            console.log(this.title, tag);          
        }, this);// en mettant this en deuxième argument il sera lié au this.title.
    }
}
````
6) Arrow function.
est le moyen le plus sur de lié le this, afin de partager le context..
   
Déclaration d'un arrow function.
````javascript
let arrow = (paramètre1, paramètre2) => {
    console.log(paramètre1, paramètre2);
};

// appel de la fonction
arrow('Jean', 12);
````

![img_11.png](img_11.png)

````javascript
const video = {
    title: 'titre',
    tags: ['tag1', 'tag2', 'tag3'],
    showTags() {
        console.log(this);// this fait réference à l'objet video        
        this.tags.forEach((tag) => { // la flèche lié le context parent à la fonction.
            console.log(this.title, tag);// ici this sera lié à la fonction parent.          
        });
    }
};
````

![img_12.png](img_12.png)

7) Méthode bind.(lié)à 22minutes


8) Méthode call


### <span style="color: #aaffff">**2. Présentation structure Angular**</span>

<span style="color: yellow">*e2e*</span>, veut dire end to end, dossier qui contient des tests end to end, ce sont des tests écris coté navigateur pour tester les réactions cotée navigateur, le html.

<span style="color: yellow">*node_modules*</span>, qui détient toutes les dépendances de notre projet

<span style="color: yellow">*angular.json*</span>, c'est un fichier qui contient les informations sur notre projet, les tyles, les scripts, les configurations, etc.

<span style="color: yellow">*package.json*</span>, on peut voir les dépendances, les types de script que l'on peut appeler(ng, start, build, test, lint, e2e)

<span style="color: yellow">*src*</span> dossier source où on n'a tout notre projet, tous les fichiers sources pour notre application.<br>
On trouve à l'intérieur :<br>
le dossier style.scss, contient tout les style de notre application.

<span style="color: yellow">*environnement*</span>, ce dossier contient nos variables d'environnement.

<span style="color: yellow">*assets*</span>, ce dossier contient les images.

<span style="color: yellow">*app*</span>, ce dossier contient notre application, dont.<br>
![img_13.png](img_13.png)

ils corespondent à un composant.(html pour la page, scss pour le style, spec pour les tests, ts pour typescript).
app.module.ts et app.routing.modules.ts
![img_14.png](img_14.png)

### <span style="color: #aaffff">**3. install bootstrap**</span>
Tapez npm install bootstrap, dans le terminal.<br>
![img_15.png](img_15.png)

Maintenant bootstrap est bien dans mes dépendances.
![img_16.png](img_16.png)
et aussi dans le dossier node_modules.

Ajoutez bootstrap dans le fichier angular.json, puis dans "styles"
![img_17.png](img_17.png)

### <span style="color: #aaffff">**4. Notre premier composant**</span>
Dans le dossier app, crée un dossier composant puis à l'intérieur crée un dossier app, sélectionné les fichiers app.component.

![img_18.png](img_18.png)

puis couper/coller et les déplacer dans le nouveau dossier app.
Vérifier dans le fichier app.module.ts qu'il importe bien nos fichiers que nous avons déplacés.
![img_19.png](img_19.png)

Dans le fichier app.component.ts.<br>
Un component est une classe que l'on exporte et qui est annoté d'une directive qui s'appelle @Component.Elle est importée à partir du core d'angular 

![img_20.png](img_20.png)

à l'intérieur du décorateur @Component, on trouve :<br>
<span style="color: yellow">*selector*</span> : il s'agit du nom qu'on utilisera comme balise HTML pour afficher ce component. Ce nom doit être unique et ne doit pas être un nom réservé HTML de type div, body, etc. On utilisera donc très souvent un préfixe comme app.
ici 'app-root'.

<span style="color: yellow">*templateUrl*</span>: le chemin vers le code HTML à injecter, './app.component.html',

![img_22.png](img_22.png)

On peut aussi écrire du html entre des backticks.

![img_21.png](img_21.png)

<span style="color: yellow">*styleUrls*</span> : un array contenant un ou plusieurs chemins vers les feuilles de styles qui concernent ce component, 'app.component.scss'.

#### <span style="color: #aaffff">Créer un component avec</span> <span style="color: yellow">**ng generate component**</span>
Tapez dans le terminal.<br>
ng generate component nomDuComponent<br>
exemple: ng generate component about<br>
Raccourci:<span style="color: yellow">*ng g c about*</span> 

Le CLI a créé un nouveau sous-dossier **about** et a créé quatre fichiers<br>
<span style="color: yellow">*about.component.html*</span>, pour la page html<br>
<span style="color: yellow">*about.component.scss*</span>, une feuille de styles <br>
<span style="color: yellow">*about.component.ts*</span>, un fichier component <br>
<span style="color: yellow">*about.component.spec.ts*</span>, un fichier spec pour les tests

### <span style="color: #aaffff">**5. Interpolation**</span>
L'interpolation c'est le fait d'afficher une variable dans le html ou une méthode grâce à deux accolades <span style="color: yellow">**{{ title }}**</span> entre les balises.

![img_25.png](img_25.png)

La variable title par interpolation.<br>
![img_23.png](img_23.png)

méthode getTitle par interpolation.<br>
![img_24.png](img_24.png)

### <span style="color: #aaffff">**6. Property binding**</span>

### <span style="color: #aaffff">**7. event binding**</span>

<span style="color: yellow">*couleur*</span>


### <span style="color: #aaffff">**8. two way data binding**</span>



### <span style="color: #aaffff">**9. a faire**</span>

### <span style="color: #aaffff">**10. a faire**</span>
### <span style="color: #aaffff">**11. a faire**</span>
### <span style="color: #aaffff">**12. a faire**</span>
### <span style="color: #aaffff">**13. a faire**</span>
### <span style="color: #aaffff">**14. a faire**</span>
### <span style="color: #aaffff">**15. a faire**</span>
### <span style="color: #aaffff">**16. a faire**</span>
### <span style="color: #aaffff">**16. a faire**</span>
### <span style="color: #aaffff">**17. a faire**</span>
### <span style="color: #aaffff">**18. a faire**</span>
### <span style="color: #aaffff">**19. a faire**</span>
### <span style="color: #aaffff">**20. a faire**</span>
### <span style="color: #aaffff">**21. a faire**</span>
