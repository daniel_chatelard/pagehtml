##  <div style="color: #26B260">**VI. Bootstrap**</div>
[Retour au sommaire](sommaire.md) 

### <span style="color: #aaffff">**1. Introduction**</span>
Bootstrap est un framework CSS, crée par Mark Otto et Jacob Thornton de la socièté Twitter Le 19 Août 2011 pour la première version.<br>

Un framework est une collection d'outils qui va constituer le "squelette" d'une application ou d'un site.<br>

Apportent un bon nombre de classes préconfigurées facilitant la mise en page et la mise en forme de l'application.<br>
  - Les aspects "layout" : gestion des colonnes, du responsive, grille d'affichage. 
  - Les aspects "contenus" : classes préconfigurées pour l'affichage du texte, des tableaux, des images.
  - L'affichage de certains composants : mise en forme des boutons, des éléments de formulaires.<br>

Tous ces éléments permettent donc de gagner en temps et en uniformité.<br>

Bootstrap est donc un framework proposant une bibliothèque d'outils HTML, CSS et JavaScript, qui vont nous aider à concevoir nos applications web.<br>  

#### <span style="color: #aaffff">**Sans installation via un CDN.**</span>

CDN (Content Delivery Network), ou réseau de diffusion de contenu (RDC) en français, est un ensemble d'ordinateurs (serveurs) mettant du contenu à disposition d'utilisateurs.<br>

Pour utiliser BootstrapCDN, on insère les chemins des fichiers dans avec la balise link dans le head de notre code HTML avant nos propres fichiers, et on insère jQuery avant le fichier JavaScript de Bootstrap le tout avant la balise de fin de body <span style="color: red"><**/body**></span>.<br>
````html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Bootstrap</title>
    <!-- Bootstrap CDN CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Votre  CSS -->
    <link href="style.css" rel="stylesheet" type="text/css">
  </head>
  <body>
    <h1>Hello world</h1>
    

    <!-- jQuery CDN -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <!-- Bootstrap CDN JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
    <!-- Our JavaScript -->
    <script src="script.js"></script>
  </body>
</html>



````
#### <span style="color: #aaffff">**Avec installation.**</span>
Pour installer Bootstrap manuellement sur nos projets, nous devons :

- Télécharger le fichier ZIP de Bootstrap et le décompresser dans notre projet.
- Télécharger jQuery et le placer dans notre projet.
- Inclure le fichier CSS de Bootstrap dans notre fichier HTML, avant nos propres feuilles de styles.  
- Inclure jQuery avant le fichier JavaScript de Bootstrap.
- Inclure le fichier JavaScript de Bootstrap avant nos fichiers JavaScript.
````html
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bootstrap</title>
        <!-- Bootstrap CSS -->
        <link href="bootstrap-4.4.1-dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Our CSS -->
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
    <div class="container">
        <h1>Hello World !</h1>
        <div class="alert alert-success" role="alert">
            Si vous voyez ce message en vert foncé dans un encart vert alors bravo, vous avez réussi à installer Bootstrap !
        </div>
    </div>
    
    <!-- jQuery -->
    <script src="jquery-3.4.1.min.js"></script>
    <!-- Bootstrap JavaScript -->
    <script src="bootstrap-4.4.1-dist/js/bootstrap.bundle.min.js"></script>
    <!-- Our JavaScript -->
    <script src="script.js"></script>
    </body>
</html>


````
### <span style="color: #aaffff">**2. La grille Bootstrap**</span>
La grille Bootstrap repose sur un système de classes CSS, avec un conteneur contenant des lignes, elles-mêmes composées de colonnes.<br>

Il y a également des classes CSS pour gérer les alignements horizontaux et verticaux ou l'ordre des colonnes.<br>

La grille de Bootstrap est une grille à 12 colonnes.<br>

La grille de Bootstrap dispose donc de douze colonnes et cinq points de rupture pour le responsive web design.<br>

Une colonne peut donc avoir une taille de 1/12, 2/12, ..., ou 12/12 de l'espace disponible.<br>

### <span style="color: #aaffff">**3. La Class container.**</span>  
La classe <span style="color: yellow">*container*</span>, englobe la totalité de la grille et contient des lignes <span style="color: yellow">*row*</span>, qui contiennent des colonnes <span style="color: yellow">*col*</span>.  

```html
    <div class="container">
    <div class="row"><!--Première ligne-->
        <div class="col"></div><!--Première colonne de la première ligne-->
        <div class="col"></div><!--Deuxième colonne de la première ligne-->
    </div>
```
![bootstrapPhoto.png](bootstrapPhoto.png)
Une ligne <span style="color: yellow">*row*</span>, contient 12 colonnes de base dans bootstrap.<br>
<span style="color: yellow">*.container*</span> : fixe une largeur maximale (max-width) à chaque point de rupture.<br>
<span style="color: yellow">*.container-fluid*</span> : largeur 100 % à tous les points de rupture.<br>
<span style="color: yellow">*.container-{breakpoint}*</span> : largeur 100 % jusqu'au point de rupture spécifié ({breakpoint}, ex : .container-sm).<br>
### <span style="color: #aaffff">**4. Les breakpoints**</span>

Les breakpoints servent à modifier les éléments de la grille en fonction des différentes taille d'écran.

1. xs extra small.(inférieur à 576px) (petit smartphone)
2. sm small.(entre 577px et 768px) (smartphone)
3. md medium.(entre 769px et 992px) (tablette)
4. lg large.(entre 992px et 1200px) (petit ordinateur)
5. xl extra large.(supérieur à 1200px) (ordinateur de bureau)

![img.png](img.png)  

exemple:
```html
    <div class="col-lg"></div>
```

### <span style="color: #aaffff">**5. Les lignes**</span>

<span style="color: yellow">*.row*</span> : classe basique pour déclarer une ligne.

<span style="color: yellow">*.row-cols-{n}*</span> : classe permettant de spécifier le nombre de colonnes (ex : <span style="color: yellow">*(.row-cols-2)*</span> pour deux colonnes). Ainsi, si l'on dispose de 5 éléments <span style="color: yellow">*(.col)*</span> dans une ligne prévue pour 2 colonnes <span style="color: yellow">*(.row-cols-2)*</span>, le rendu visuel sera de trois lignes (deux lignes de deux colonnes et une ligne avec la dernière).

<span style="color: yellow">*.row-cols-{breakpoint}-{n}*</span> : permet de spécifier le nombre de colonnes pour un breakpoint donné (ex : <span style="color: yellow">*(.row-cols-sm-2)*</span>.<br>

### <span style="color: #aaffff">**6. Les colonnes**</span>

<span style="color: yellow">*.col*</span> : classe de base pour les colonnes, elle occupera toute la place disponible en fonction de la présence ou non d'autres colonnes (ex : toute la place si elle est seule, la moitié s'il y en a deux, etc.).

<span style="color: yellow">*.col-{n}*</span> : permet de spécifier le nombre de colonnes que nous voulons utiliser parmi les douze possibles par ligne. Par exemple, si nous voulons 2 colonnes, dont une est beaucoup plus grande, nous pouvons lui donner la classe <span style="color: yellow">*.col-10*</span>.

<span style="color: yellow">*.col-{breakpoint}*</span> : les points de rupture de la grille sont basés sur des medias queries de largeur minimale, c'est-à-dire qu'ils s'appliquent à ce point de rupture et à tous ceux qui se trouvent au-dessus (par exemple, <span style="color: yellow">*.col-sm-4*</span>  s'applique aux appareils de petite, moyenne, grande et très grande taille, mais pas au premier point de rupture xs).

<span style="color: yellow">*.col-{breakpoint}-{n}*</span> : permet de spécifier un nombre de colonnes en fonction d'un point de rupture donné.

### <span style="color: #aaffff">**7. Les classes utilities flexbox**</span>
<span style="color: yellow">*.justify-content-**</span> : pour modifier l'alignement des éléments flex sur l'axe principal (l'axe x ou l'axe y si flex-direction est en colonnes). Il s'agit de la propriété CSS justify-content, les valeurs sont donc les mêmes : start (valeur par défaut des navigateurs), end, center, between, ou around (ex : <span style="color: yellow">*justify-content-between*</span>.
- start, aligne le bloc au début.
- end, aligne le bloc à la fin.
- center, aligne le bloc au centre.
- between, met des espaces seulement entre les blocs.
- around, met des espaces entre les blocs, mais aussi au début et à la fin.
````html
<body>
    <div class="container">
        <div class="row justify-content-around">
            <div class="col-2">Taille 2</div>
            <div class="col-2">taille 2</div>
            <div class="col-2">taille 2</div>
        </div>
    </div>
</body>
````
<span style="color: yellow">*.justify-content-{breakpoint}-**</span> : permet de spécifier un point de rupture.
````html
<body>
    <div class="container-fluid"><!--Prend toute la largeur de la page-->
        <div class="row justify-content-sm-center"><!-- sm, pour small en responsive-->
            <div class="col-2">Taille 2</div>
            <div class="col-2">taille 2</div>
            <div class="col-2">taille 2</div>
        </div>
    </div>
</body>
````
<span style="color: yellow">*.align-items-*</span>* : permet de modifier l'alignement des objets flex sur l'axe secondaire (l'axe y ou l'axe x si flex-direction est en colonnes). Il s'agit de la propriété CSS align-items, les valeurs sont donc les mêmes : start, end, center, baseline, ou stretch (valeur par défaut des navigateurs). Il est également possible de spécifier un breakpoint <span style="color: yellow">*align-items-{breakpoint}-**</span>.
````html
<body>
    <div class="container-fluid"><!--Prend toute la largeur de la page-->
        <div class="row align-items-start"><!-- alignement verticale d'une ligne en haut du bloc (div class ="row").-->
            <div class="col-2 align-self-end">Taille 2</div><!--alignera cette colonne en bas de la ligne (row).-->
            <div class="col-2 align-self-center">taille 2</div>
            <div class="col-2">taille 2</div>
        </div>
    </div>
</body>
````

<span style="color: yellow">*.align-self-*</span>* : permet de modifier l'alignement d'un objet flex en particulier sur l'axe secondaire (l'axe y ou l'axe x si flex-direction est en colonnes). Cette classe doit être affectée sur l'élément lui-même. Il est également possible de spécifier un breakpoint <span style="color: yellow">*.align-self-{breakpoint}-**</span>.
````html
<body>
    <div class="container-fluid"><!--Prend toute la largeur de la page-->
        <div class="row align-items-start"><!-- alignement verticale d'une ligne en haut du bloc (div class ="row").-->
            <div class="col-2 align-self-end">Taille 2</div><!--alignera cette colonne en bas de la ligne (row).-->
            <div class="col-2 align-self-sm-center">taille 2</div><!--avec du responsive (-sm).-->
            <div class="col-2">taille 2</div>
        </div>
    </div>
</body>
````
### <span style="color: #aaffff">**8. Les listes.**</span>
- Pour supprimer les styles sur les listes, il faut utiliser la classe <span style="color: yellow">*list-unstyled*</span>.<br>
````html
    <ul class="list-unstyled">// <!--supprime les puces devant le texte.-->
        <li>mon premier li</li>
        <li>mon second li</li>
        <li>mon troisième li
            <ul><!--liste à puces de base, avec les petits rond.-->
                <li>deuxième ul, mon premier li</li>
                <li>deuxième ul, mon deuxième li</li>
                <li>deuxième ul, mon troisième li</li>
            </ul>
        </li>
    </ul>
````
![Unstyled-list.png](Unstyled-list.png)

- Pour créer une liste d'éléments en lignes, il faut ajouter la classe <span style="color: yellow">*list-inline*</span> à l'élément ul, et <span style="color: yellow">*list-inline-item*</span> à chaque élément li.<br>
````html
    <ul class="list-inline"><!-- met les puces les un à la suite des autres, horizontalemment.-->
      <li class="list-inline-item">premier élément</li>
      <li class="list-inline-item">deuxième élément</li>
      <li class="list-inline-item">troisième élément</li>
    </ul>
````
![liste-inline.png](liste-inline.png)
-  Pour créer une liste d'éléments sous forme de menu, il faut ajouter la classe <span style="color: yellow">*list-group*</span> à l'élément ul, et <span style="color: yellow">*list-group-item*</span> à chaque élément li.<br>

````html
    <ul class="list-group">
      <li class="list-group-item">Premier élément</li>
      <li class="list-group-item">Deuxième élément</li>
      <li class="list-group-item">Troisième élément</li>
      <li class="list-group-item">Quatrième élément</li>
    </ul>   
````
![liste-groupe.png](liste-groupe.png)

````html
<!-- Ajout de couleur sur list-group-item -->
    <ul class="list-group">
      <li class="list-group-item-success">Premier élément</li>
      <li class="list-group-item-danger">Deuxième élément</li>
      <li class="list-group-item-warning">Troisième élément</li>
      <li class="list-group-item">Quatrième élément</li>
    </ul>   
````
![liste-groupe-color.png](liste-groupe-color.png)
### <span style="color: #aaffff">**9. Les tableaux.**</span>

<span style="color: yellow">*.table*</span>, permet de styliser un tableau, exemple ci-dessous.
![table_1.png](table_1.png)
````html
    <table class="table">
        <thead>
            <tr>
                <th>Entête 1</th>
                <th>Entête 2</th>
                <th>Entête 3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
        </tbody>
    </table>
````
<span style="color: yellow">*.table-dark*</span>, pour un tableau en fond noir dans la class de la balise (table), exemple ci-dessous.
![table-dark_1.png](table-dark_1.png)
````html
    <table class="table table-dark">
        <thead>
            <tr>
                <th>Entête 1</th>
                <th>Entête 2</th>
                <th>Entête 3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
        </tbody>
    </table>
````
<span style="color: yellow">*.thead-light*</span>,met l'en-têtes des colonnes en clair, exemple ci-dessous.
![table-thead-light.png](table-thead-light.png)
````html
    <table class="table">
        <thead class="thead-light"><!-- Ou thead-dark, pour mettre en foncé.-->
            <tr>
                <th>Entête 1</th>
                <th>Entête 2</th>
                <th>Entête 3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
        </tbody>
    </table>
````
<span style="color: yellow">*.thead-dark*</span>,met l'en-têtes des colonnes en foncé.

<span style="color: yellow">*.table-striped*</span>, une ligne sur deux sera coloré, exemple ci-dessous.
![table_1.png](table-striped.png)
````html
    <table class="table table-striped">
        <thead class="thead-dark"><!-- en-tête en foncé -->
            <tr>
                <th>Entête 1</th>
                <th>Entête 2</th>
                <th>Entête 3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
        </tbody>
    </table>
````
<span style="color: yellow">*.table-hover*</span>, colorie au survol de la souris.
````html
    <table class="table table-hover"><!-- au survole de la souris. -->
        <thead>
            <tr>
                <th>Entête 1</th>
                <th>Entête 2</th>
                <th>Entête 3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
        </tbody>
    </table>
````
<span style="color: yellow">*.table-primary*</span>, colori le fond du tableau.
````html
    <table class="table table-primary"><!-- couleur de fond du tableau. -->
        <thead>
            <tr>
                <th>Entête 1</th>
                <th>Entête 2</th>
                <th>Entête 3</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
            <tr>
                <td>Valeur 1</td>
                <td>Valeur 2</td>
                <td>Valeur 3</td>
            </tr>
        </tbody>
    </table>
````
### <span style="color: #aaffff">**10. Les boutons.**</span>
La classe de base pour le style des boutons est <span style="color: yellow">*.btn*</span> 
````html
<button type="button" class="btn">Bouton</button>
````
<span style="color: yellow">*.btn-warning*</span>, pour remplir de couleur un bouton.
````html
<button type="button" class="btn btn-warning">Bouton</button>
````
![bouton-wraning.png](bouton-wraning.png)

<span style="color: yellow">*.btn-outline*</span>, colorie le texte et le contour.
![Buttons-outline.png](Buttons-outline.png)
````html
    <button type="button" class="btn btn-outline-primary">Primary, bleu</button>
    <button type="button" class="btn btn-outline-secondary">Secondary, gris</button>
    <button type="button" class="btn btn-outline-success">Success, vert</button>
    <button type="button" class="btn btn-outline-danger">Danger, rouge</button>
    <button type="button" class="btn btn-outline-warning">Warning, orange</button>
    <button type="button" class="btn btn-outline-info">Info, bleu ciel</button>
    <button type="button" class="btn btn-outline-light">Light, blanc</button>
    <button type="button" class="btn btn-outline-dark">Dark, noir</button>
````
<span style="color: yellow">*.btn-lg ou .btn-sm*</span>, taille des boutons (lg, large)(sm, small) etc....
![bouton-size.png](bouton-size.png)

<span style="color: yellow">*.btn-group*</span>, pour grouper les boutons.
![bouton-groupe.png](bouton-groupe.png)
````html
    <div class="btn-group">
        <button type="button" class="btn btn-warning">bouton gauche</button>
        <button type="button" class="btn btn-danger">bouton milieu</button>
        <button type="button" class="btn btn-success">bouton droite</button>
    </div>
````
En remplaçant <span style="color: yellow">*.btn-group*</span> par<span style="color: yellow">*.btn-group-vertical*</span> , on obtient un groupe de boutons alignés verticalement.


### <span style="color: #aaffff">**11. Les formulaires.**</span>
Par défaut, Bootstrap applique le style display: block à la majorité des éléments des formulaires.<br>

<span style="color: yellow">*.form-control*</span>, permet de styliser et d'afficher l'élément sur toute la largeur du formulaire, appliqué à la balise (input).

<span style="color: yellow">*.form-group*</span>, groupe un label et un champ de formulaire.
````html
    <div class="form-group">
          <label>Element de formulaire</label>
          <input type="text" class="form-control" id="element" placeholder="Mon placeholder">
    </div>
````
![form.png](form.png)

<span style="color: yellow">*.form-control-lg*</span>, gérer le dimensionnement et la hauteur des champs.<br>
<span style="color: yellow">*.form-control-md*</span>, gérer le dimensionnement et la hauteur des champs, valeur par défaut.<br>
<span style="color: yellow">*.form-control-sm*</span>, gérer le dimensionnement et la hauteur des champs.<br>
![form-size.png](form-size.png)
````html
    <div class="form-group">
        <label for="username">.form-control-lg</label>
        <input type="text" class="form-control form-control-lg" id="username">
        <label for="password">.form-control-sm</label>
        <input type="password" class="form-control form-control-sm" id="password">
    </div>
````

<span style="color: yellow">*.form-check*</span>, les champs de type radio ou checkbox d'un formulaire sont par défaut affichés les uns en dessous des autres.
````html
    <!-- form-check -->
    <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="check">
        <label class="form-check-label" for="check">
            une première checkbox
        </label>
    </div>
    <div class="form-check">
        <input class="form-check-input" type="checkbox" value="" id="check">
        <label class="form-check-label" for="check">
            une seconde checkbox
        </label>
    </div>      
````
![checkbox.png](checkbox.png)
<span style="color: yellow">*.form-check-inline*</span>, les champs de type radio ou checkbox d'un formulaire seront afficher en ligne.
````html
    <!-- form-check-inline-->
    <div class="form-check-inline">
        <input class="form-check-input" type="checkbox" value="" id="check">
        <label class="form-check-label" for="check">
            une première checkbox inline
        </label>
    </div>
    <div class="form-check-inline">
        <input class="form-check-input" type="checkbox" value="" id="check">
        <label class="form-check-label" for="check">
            une seconde checkbox inline
        </label>
    </div>
````
![checkbox-inline.png](checkbox-inline.png)
### <span style="color: #aaffff">**12. Les icônes.**</span>
Bootstrap 4 possède sa propre librairie d'icônes intégrées. Ces icônes sont téléchargeables et sont au format **svg**.<br>
Pour utiliser les icônes, il suffit de cliquer sur une icône de la librairie et de copier-coller le code.
````html
    <button type="button" class="btn btn-danger">
        Delete
        <!-- code à copier sur Bootstrap-->
        <svg class="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path d="M5.5 5.5A.5.5 0 016 6v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm2.5 0a.5.5 0 01.5.5v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm3 .5a.5.5 0 00-1 0v6a.5.5 0 001 0V6z"/>
          <path fill-rule="evenodd" d="M14.5 3a1 1 0 01-1 1H13v9a2 2 0 01-2 2H5a2 2 0 01-2-2V4h-.5a1 1 0 01-1-1V2a1 1 0 011-1H6a1 1 0 011-1h2a1 1 0 011 1h3.5a1 1 0 011 1v1zM4.118 4L4 4.059V13a1 1 0 001 1h6a1 1 0 001-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" clip-rule="evenodd"/>
        </svg>
        <!-- et à coller -->
    </button>
````
![delete-button-icon.png](delete-button-icon.png)

Il est possible de changer la taille de l’icône de plusieurs façons :

- Via les attributs width et height de la balise du svg (cf exemple)  
- Via le CSS
````css
    .bi-trash {
       width: 1em;
       height: 1em;
    }
````
<span style="color: yellow">*.text-danger*</span>, met la couleur rouge sur icône.
````html
    <button type="button" class="btn btn-danger">
        Delete
        <!-- code à copier sur Bootstrap-->
        <svg class="bi bi-trash text-danger" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path d="M5.5 5.5A.5.5 0 016 6v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm2.5 0a.5.5 0 01.5.5v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm3 .5a.5.5 0 00-1 0v6a.5.5 0 001 0V6z"/>
          <path fill-rule="evenodd" d="M14.5 3a1 1 0 01-1 1H13v9a2 2 0 01-2 2H5a2 2 0 01-2-2V4h-.5a1 1 0 01-1-1V2a1 1 0 011-1H6a1 1 0 011-1h2a1 1 0 011 1h3.5a1 1 0 011 1v1zM4.118 4L4 4.059V13a1 1 0 001 1h6a1 1 0 001-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z" clip-rule="evenodd"/>
        </svg>
        <!-- et à coller -->
    </button>
````
### <span style="color: #aaffff">**13. Les images.**</span>
Pour rendre une image responsive, Bootstrap 4 propose une classe :<br>
<span style="color: yellow">*.img-fluid*</span>. la taille de l'image s'adaptera à la taille de l'écran, sauf si l'image est plus petite que l'écran..
````html
    <div>
        <img src="#" class="img-fluid">
    </div>
````
Changer l'apparence des bordures des images :<br>
<span style="color: yellow">*.img-thumbnail*</span> : affecte un liseré autour de l'image, effet de cadre.<br>
````html
    <div>
        <img src="#" class="img-thumbnail">
    </div>
````
<span style="color: yellow">*.rounded*</span> : arrondit les coins de l'image.<br>
````html
    <div>
        <img src="#" class="rounded">
    </div>
````
<span style="color: yellow">*.rounded-top*</span>, <span style="color: yellow">*.rounded-bottom*</span>, <span style="color: yellow">*.rounded-left*</span>, <span style="color: yellow">*.rounded-right*</span> : arrondissent les coins à l'emplacement défini par la classe.<br>
<span style="color: yellow">*.rounded-circle*</span> : permet d’afficher l'image dans un cercle.<br>
````html
    <div>
        <img src="#" class="rounded-circle">
    </div>
````
### <span style="color: #aaffff">**14. Menu de navigation.**</span>

<span style="color: yellow">*.navbar*</span> : pour déclarer le composant pour le menu de navigaation.
````html
    <boby>
        <!-- déclaration du menu avec navbar -->
        <nav class="navbar">
            
        </nav>
    </boby>
````
<span style="color: yellow">*navbar-dark bg-primary*</span>, modifier la couleur du menu appliquées sur l'élément <span style="color: #ff00b9"><**nav**></span>.
````html
    <boby>
        <!-- navbar-dark (texte en blanc) bg-primary (fond bleu) -->
        <nav class="navbar navbar-dark bg-primary">
            
        </nav>
    </boby>
````
![nav.png](nav.png)

<span style="color: yellow">*.navbar-brand*</span> : correspondra généralement au nom du site ou logo.
````html
    <boby>
        <!-- navbar-brand dans la balise (a) -->
        <nav class="navbar navbar-dark bg-primary">
            <a href="#" class="navbar-brand">logo ou nom du site</a>
        </nav>
    </boby>
````
<span style="color: yellow">*.navbar-nav*</span> : pour dire à bootstrap que c'est mon élément de menu.
````html
    <boby>
        <!-- navbar-nav dans la balise (ul)-->
        <nav class="navbar navbar-dark bg-primary">
            <a href="#" class="navbar-brand">logo ou nom du site</a>
            <ul class="navbar-nav">
                
            </ul>
        </nav>
    </boby>
````
<span style="color: yellow">*.nav-item*</span>: pour déclarer un élément de navigation, généralement suivi d'un lien disposant de la classe <span style="color: yellow">*.nav-link*</span>.
````html
    <boby>
        <!-- nav-item dans la balise (li) et nav-link dans la balise (a) pour le lien-->
        <nav class="navbar navbar-dark bg-primary">
            <a href="#" class="navbar-brand">logo ou nom du site</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">Home</a>
                    <a href="#" class="nav-link">Compétences</a>
                    <a href="#" class="nav-link">Projets</a>
                    <a href="#" class="nav-link">Contact</a>
                </li>
            </ul>
        </nav>
    </boby>
````
<span style="color: yellow">*.navbar-expand-lg*</span>, pour le responsive.(lg, écran d'ordinateur portable).Change l'affichage en dessous de se point de rupture.
````html
    <boby>
        <!-- navbar-expand-lg dans la balise nav-->
        <nav class="navbar navbar-dark bg-primary navbar-expand-lg">
            <a href="#" class="navbar-brand">logo ou nom du site</a>
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a href="#" class="nav-link">Home</a>
                    <a href="#" class="nav-link">Compétences</a>
                    <a href="#" class="nav-link">Projets</a>
                    <a href="#" class="nav-link">Contact</a>
                </li>
            </ul>
        </nav>
    </boby>
````
<span style="color: yellow">*.collapse.navbar-collapse*</span> : pour gérer le menu dans sa version responsive.
````html
    <boby>
        <!-- collapse navbar-collapse dans la balise (div) qui englobe (ul)-->
        <nav class="navbar navbar-dark bg-primary navbar-expand-lg">
            <a href="#" class="navbar-brand">logo ou nom du site</a>
            <div class="collapse navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link">Home</a>
                        <a href="#" class="nav-link">Compétences</a>
                        <a href="#" class="nav-link">Projets</a>
                        <a href="#" class="nav-link">Contact</a>
                    </li>
                </ul>
            </div>    
        </nav>
    </boby>
````
<span style="color: yellow">*ajout d'un bouton et d'un icons*</span> : 
````html
    <boby>
        <!-- ajout d'un bouton et d'un icons-->
        <nav class="navbar navbar-dark bg-primary navbar-expand-lg">
            <a href="#" class="navbar-brand">logo ou nom du site</a>
            <button 
                type="button"
                class="navbar-toggler"
                data-toggle="collapse"
                data-target="#navbarNavDropdown"
                aria-controls="#navbarNavDropdown"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
             <!-- icons Three dots vertical -->   
             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                    <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
             </svg>    
            </button>
            <div class="collapse navbar-collapse" id="#navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active">Home</a>
                        <a href="#" class="nav-link">Compétences</a>
                        <a href="#" class="nav-link">Projets</a>
                        <a href="#" class="nav-link">Contact</a>
                    </li>
                </ul>
            </div>    
        </nav>
    </boby>
````
<span style="color: yellow">*.navbar-text*</span> : pour ajouter du texte qui ne serait pas un lien de navigation


<span style="color: yellow">*.breadcrumb*</span>, pour initialiser le composant
````html
    <!-- breadcrumb, mis dans (nav et ol) -->
    <div class="row">
        <nav class="col-12" aria-label="breadcrumb">
            <!-- Déclaration du composant -->
            <ol class="breadcrumb">
                <!-- Item du fil d'ariane -->
                <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                <li class="breadcrumb-item active" aria-current="page">Mon espace</li>
            </ol>
        </nav>
    </div>
````
<span style="color: yellow">*.breadcrumb-item*</span>, pour déclarer une étape dans le cheminement
````html
    <boby>
        <!-- ajout d'un bouton et d'un icons-->
        <nav class="navbar navbar-dark bg-primary navbar-expand-lg">
            <a href="#" class="navbar-brand">logo ou nom du site</a>
            <button 
                type="button"
                class="navbar-toggler"
                data-toggle="collapse"
                data-target="#navbarNavDropdown"
                aria-controls="#navbarNavDropdown"
                aria-expanded="false"
                aria-label="Toggle navigation"
            >
             <!-- icons Three dots vertical -->   
             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                    <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z"/>
             </svg>    
            </button>
            <div class="collapse navbar-collapse" id="#navbarNavDropdown">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link active">Home</a>
                        <a href="#" class="nav-link">Compétences</a>
                        <a href="#" class="nav-link">Projets</a>
                        <a href="#" class="nav-link">Contact</a>
                    </li>
                </ul>
            </div>    
        </nav>
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="#">Home</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">Science</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="#">2018</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page">
                    Molecular structures made simple
                </li>
            </ol>            
        </nav>
    </boby>
````
### <span style="color: #aaffff">**15. Les cartes.**</span>

<span style="color: yellow">*.card*</span> : pour initialiser le composant
````html
<!-- card, pour crée un carte -->
<div class="container"><!-- contient le tout -->
    <div class="card">
        
    </div>
</div>
````
<span style="color: yellow">*.card-img-top*</span> : pour placer une image en haut de la carte.
````html
    <!-- card-img-top, pour l'image en haut -->
    <div class="container"><!-- contient le tout -->
        <div class="card">
            <img src="#" alt="" class="card-img-top">
        </div>
    </div>
````
<span style="color: yellow">*.card-body*</span> : le bloc contenu principal de la carte
````html
<!-- card-body, pour mettre du texte -->
<div class="container">
    <div class="card">
        <img src="#" alt="" class="card-img-top">
        <div class="card-body">
                        
        </div>     
    </div>
</div>
````
<span style="color: yellow">*.card-title*</span> : pour le titre de la carte
````html
<!-- card-title, pour mettre le titre -->
<div class="container">
    <div class="card">
        <img src="#" alt="" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">titre</h5>
                        
        </div>     
    </div>
</div>
````
<span style="color: yellow">*.card-text*</span> : pour le texte de la carte.
````html
<!-- card-text, pour mettre le texte que l'on souhaite -->
<div class="container">
    <div class="card">
        <img src="#" alt="" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">titre</h5>
            <p class="card-text">texte</p>
            <button type="button" class="btn btn-primary">ok</button>
            
        </div>     
    </div>
</div>
````
<span style="color: yellow">*button*</span> : ajout d'un bouton valider.
````html
<!-- ajout d'un bouton, pour valider -->
<div class="container">
    <div class="card">
        <img src="#" alt="" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">titre</h5>
            <p class="card-text">texte</p>
            <button type="button" class="btn btn-primary">valider</button>            
        </div>     
    </div>
</div>
````
<span style="color: yellow">*avec un lien en forme de bouton*</span> : avec la class btn et btn-secondary, pour la couleur du bouton.
````html
    <div class="row">
        <h2 class="col-12">Mes actus</h2>
        <div class="col-4">
            <!-- Déclaration du composant -->
            <div class="card">
                <!-- Image située en haut -->
                <img src="https://cdn.pixabay.com/photo/2014/12/16/22/25/woman-570883_960_720.jpg" class="card-img-top" alt="Méditation">
                <!-- Corps de la carte -->
                <div class="card-body">
                    <!-- Titre -->
                    <h5 class="card-title">La méditation, bonne pour la santé</h5>
                    <!-- Contenu de la carte -->
                    <p class="card-text">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Pellentesque semper mi est, ac molestie nibh laoreet in.
                        Pellentesque massa orci, vulputate et condimentum vitae, pellentesque eget ipsum.
                    </p>
                    <a href="#" class="btn btn-secondary">Lire la suite...</a>
                </div>
            </div>
        </div>
        <!-- Deux autres cartes -->
    </div>
````

### <span style="color: #aaffff">**16. Les paginations.**</span>
<span style="color: yellow">*.pagination*</span>: permet d'initialiser le composant
````html
    <!-- pagination sur ul. -->
    <nav aria-label="Pagination">
        <ul class="pagination">
            
        </ul>
        
    </nav>
````
<span style="color: yellow">*.page-item*</span> : permet d'indiquer qu'il s'agit d'un élément de navigation
````html
    <!-- page-item sur li. -->
    <nav aria-label="Pagination">
        <ul class="pagination">
            <li class="page-item">
                
            </li>
        </ul>
        
    </nav>
````
<span style="color: yellow">*.page-link*</span> : permet d'indiquer qu'il s'agit d'un lien vers une page
````html
    <!-- page-link sur le lien (a). -->
    <nav aria-label="Pagination">
        <ul class="pagination">
            <li class="page-item">
                <a href="#" class="page-link">
                    <svg>icons chevron-left à mettre</svg>
                    Précédent
                </a>
                <a href="#" class="page-link">
                    1
                </a>
                <a href="#" class="page-link">
                    2
                </a>
                <a href="#" class="page-link">
                    3
                </a>
                <a href="#" class="page-link">
                    Suivant
                    <svg>icons chevron-right à mettre</svg>
                </a>
            </li>
        </ul>        
    </nav>
````
<span style="color: yellow">*.disabled*</span> : permet d'empêcher d'accéder à un élément

<span style="color: yellow">*.active*</span> : pour indiquer la page courante

### <span style="color: #aaffff">**17. Les barres de progress.**</span>

<span style="color: yellow">*.progress*</span> permet d'initialiser le composant
````html
    <!-- progress. -->
    <div class="progress">
                
    </div>
````
<span style="color: yellow">*progress-bar*</span> permet de matérialiser l'avancement de la barre de progrès. On définira sa taille grâce à la propriété CSS (width). Il est possible d'ajouter un label en l'intégrant au sein de la div intégrant cette même classe.
````html
    <!-- progress-bar. -->
    <div class="progress">
        <div
            role="progressbar"
            class="progress-bar bg-success progress-bar-striped"<!-- bg-success, couleur de la barre en vert, progress-bar-striped, la barre aura des rayures -->
            style="width: 90%;"
            aria-valuenow="9000"<!-- les attributs aria, servent pour accessibilité et les liseuses d'écran -->
            aria-valuemin="0"
            aria-valuemax="10000"
        >
            9000/1000 <!-- texte affiché dans la barre -->
        </div>
        
    </div>
````
### <span style="color: #aaffff">**18. Les fenêtres modales.**</span>
Elle servent à afficher un message dans une popup.<br>
<span style="color: yellow">*.modal*</span> : pour initialiser le composant
````html
    <div 
       class="modal">
       tabindex="-1"<!-- évite de la cibler avec la touche tab -->
       role="dialog"
       id="monid"
    </div>
````
<span style="color: yellow">*.modal-dialog*</span> et <span style="color: yellow">*.modal-content*</span>  : pour initialiser la fenêtre en elle-même
````html
    <div 
       class="modal">
       tabindex="-1"<!-- évite de la cibler avec la touche tab -->
       role="dialog"
       id="monid"
    <!-- modal-dialog-centered, pour centrer la modal sur la page -->
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">titre</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <!--( &times;) déssine une croix -->
                </button>
            </div>
            <div class="modal-body">
                confirmez-vous?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-success">Confirmer</button>
            </div>
        </div>
    </div>
    </div>
````
<span style="color: yellow">*.modal-header, .modal-body et .modal-footer*</span> : pour définir la structure de la fenêtre

### <span style="color: #aaffff">**19. Spacing.**</span>
[getbootstrap-spacing](https://getbootstrap.com/docs/5.0/utilities/spacing/)

<span style="color: yellow">*my*</span> pour margin, marge extérieur de l'axe y.  
<span style="color: yellow">*mx*</span> pour margin,marge extérieur de l'axe  x.  
<span style="color: yellow">*py*</span> pour padding,marge intérieur de l'axe y.  
<span style="color: yellow">*px*</span> pour padding,marge intérieur de l'axe x.

### <span style="color: #aaffff">**20.offset**</span>

<span style="color: yellow">*offset*</span> crée un décalage.<br>
````html

    <!-- offset, décalage -->
    <div class="container">
        <div class="row">
            <div class="col-md-4">.col-md-4</div>
            <div class="col-md-4 offset-md-4">.col-md-4 .offset-md-4</div>
        </div>
        <div class="row">
            <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
            <div class="col-md-3 offset-md-3">.col-md-3 .offset-md-3</div>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3">.col-md-6 .offset-md-3</div>
        </div>
    </div>


````
[getbootstrap-offset](https://getbootstrap.com/docs/5.0/layout/columns/#offsetting-columns)

La propriété est définie par :

    m - pour les classes qui définissent la marge externe ;

    p - pour les classes qui définissent la marge interne.

Pour le côté, on utilise les lettres suivantes :

    t - pour les classes qui règlent le haut de la marge ;

    b - pour les classes qui règlent le bas de la marge ;

    l - pour les classes qui règlent la marge gauche ;

    r - pour les classes qui règlent la marge droite ;

    x - pour les classes qui règlent les marges horizontales (gauche et droite) ;

    y - pour les classes qui règlent les marges verticales (haut et bas) ;

    laissez vide - pour les classes qui règlent à la fois les marges verticales et horizontales.

La taille est définie numériquement :

    0 - pour les classes qui suppriment la marge, la réglant sur 0 ;

    1 - (par défaut) pour les classes qui règlent la marge sur 0,25 rem ;

    2 - (par défaut) pour les classes qui règlent la marge sur 0,5 rem ;

    3 - (par défaut) pour les classes qui règlent la marge sur 1 rem (marge de base Bootstrap) ;

    4 - (par défaut) pour les classes qui règlent la marge sur 1,5 rem ;

    5 - (par défaut) pour les classes qui règlent la marge sur 3 rem ;

    auto - pour les classes qui règlent la marge sur auto.

Par exemple :

    Pour régler la marge externe du haut d'un élément sur 1,5 rem, ajoutez la classe   .mt-4  (m pour marge, t pour top (haut), 4 pour 1,5 rem).

    Pour régler la marge interne de tous les côtés d'un élément sur 1 rem, ajoutez la classe   .p-3  (p pour padding (marge interne), 3 pour 1 rem).

    Pour régler la marge interne verticale d'un élément sur 0, ajoutez la classe   .py-0  (p pour padding (marge interne), y pour haut et bas, c'est-à-dire les deux côtés dans le sens y, et 0 pour… 0).

    Pour régler la marge externe horizontale d'un élément sur auto, ajoutez la classe   .mx-auto  (m pour marge, x pour gauche et droite, c'est-à-dire les deux côtés dans le sens x et auto pour… auto).

[Retour au sommaire](sommaire.md)