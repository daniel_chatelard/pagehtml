##  <div style="color: #26B260">**IV. CSS**</div>
[Retour au sommaire](sommaire.md)  
Dans les années 2000, Hakon Wium Lie proposa un nouveau langage permettant d'habiller les sites à l'aide de propriétés : le CSS (Cascading Style Sheets, qui signifie « feuilles de style en cascade »).<br>


````css
    /* Permet d'indiquer que l'on cible les titres h1 */
    h1 {
    /* donne l'instruction d'afficher le texte de la couleur définie, à savoir rouge */
    color: red;
        /* commentaire en CSS */
}
````
<span style="color:#26f260;">**1. Aplliquer du style aux éléments**</span><br>

<span style="color:#26f260;">Style interne (balise style)</span>

Pour appliquer du style à un élément HTML, on peut placer le code CSS dans une balise <span style="color:red;"><**style**></span> située dans l'en-tête <span style="color:red;"><**head**></span>.

Cela va permettre de définir un style interne à la page.
````html
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre</title>
            <meta charset="utf-8" />
            <style>
                <!-- Nous ajouterons le style CSS ici -->
            </style>
        </head>
        <!-- commentaire en HTML -->
    </html>
````

<span style="color:#26f260;">Style inline</span>

Il existe une autre manière d'appliquer du style CSS en déclarant le style dans l'attribut <span style="color:#26f260;">Style style=" "</span>  d'un élément HTML, et en donnant les propriétés souhaitées dans cet attribut. Il s'agit d'une déclaration de style dite inline.<br>
Mais c'est une mauvaise pratique.

<span style="color:#26f260;">Style externe.</span><br>

Créer un fichier css, appelé style.css pratique conseillé.<br>
Lié le fichier css au fichier html avec la balise <span style="color:red;"><**link**</span> <span style="color:#26f260;">rel="stylesheet" href="style.css"></span> pour l'importe
le css, permet de styliser le html.<br>
````html
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre</title>
            <meta charset="utf-8" />
            <!-- lié le css au html -->
            <link rel="stylesheet" type="text/css" href="style.css">
        </head>        
    </html>
````
<span style="color:#26f260;">h1</span> est le sélecteur.<br>
````css
     h1 {
        color: red;
        /* commentaire en CSS */
}
````
<span style="color:#70F3EF;">color</span> = propriété, <span style="color:lightgreen;">red ou #ff0000 </span> = valeur de la propriété (couleur du texte en rouge).<br>
<span style="color:#26f260;">**2. Aplliquer du style aux class.**</span><br>
Pour associer un style à une classe, on utilise le nom de la classe précédé d'un point.
````css
    .maclasse {
        color: green;
    }
````

Il est possible de cibler élément avec <span style="color:#70F3EF;"># et un identifiant</span> (attribut <span style="color:lightgreen;">id=" "</span>), cet identifiant est unique par page. 

````css
    #identifiant {
        color: green;
    }
````
<span style="color:#26f260;">**3. Les unités CSS.**</span><br>
<span style="color:#26f260;">*Unité px.*</span><br>
C'est une unité absolue, utilisée pour définir une longueur, largeur, un padding, une margin, la taille d'un texte.<br>
````css
    .parent {
    font-size: 18px;
}
````
Les unités absolues : ne dépends d'aucune autre dimension du contenu HTML.<br>

Les unités relatives : vont varier en fonction des dimensions de l'élément HTML parent.

<span style="color:#26f260;">*Unité en em.*</span><br>
C'est une unité relative. Elle permet de définir la taille d'un texte de façon proportionnelle à son élément parent.
````css
    .enfant {
    font-size: 2em;
}
````
La dimension du texte contenu avec la class « enfant » est égale à celle du parent multipliée par 2, soit (18px * 2) = 36 px.

<span style="color:#26f260;">*Unité en rem.*</span><br>
C'est une unité relative. Elle fonctionne de la même manière que l'unité em, sauf qu'elle n'est pas proportionnelle à son élément parent, mais à l'élément racine HTML.<br>
Si la police de l’élément HTML est définie à 16 px, 1 rem sera équivalent à 16 px pour tous les éléments du document.
````css
    html {
    font-size: 20px;
    }

    div#premier {
    font-size: 0.5rem; //10px
    }
````
La balise div ayant pour id respectif premier a une taille définie en rem, elle sera donc proportionnelle à la taille de la balise racine (20 px).

0.5 rem correspond donc ici à 10 px.

<span style="color:#26f260;">*Unité vw (viewport width).*</span><br>
C'est une unité relative proportionnelle à la largeur de la fenêtre d'affichage du navigateur. 1 vw est égal à 1 % de la largeur de la fenêtre.
````css
    div#premier {
    font-size: 10vw; //10 % de 1000 px, soit 100 px
    }

    div#second {
        font-size: 5vw;// 5 % de 1000 px, soit 50 px
    }
````
Si la largeur de la fenêtre est de 1000 px.<br>
Le texte contenu dans la première div sera 10 % de 1000 px, soit 100 px.<br>
Le texte contenu dans la deuxième div sera 5 % de 1000 px, soit 50 px.<br>

<span style="color:#26f260;">*Unité vh (viewport height).*</span><br>
C'est une unité relative proportionnelle à la hauteur de la fenêtre d'affichage du navigateur.<br>
1 vh est égal à 1 % de la hauteur de la fenêtre. Les calculs seront les mêmes qu'avec vw, mais en se basant sur la hauteur de la fenêtre au lieu de la largeur.<br>

<span style="color:#26f260;">**4. L'espacement en CSS.**</span><br>
<span style="color:#70F3EF;">line-height</span>, donner une hauteur à une ligne.<br>
````css
    .ligne {
    line-height: 50px;
    }   
````

<span style="color:#70F3EF;">letter-spacing </span>, définit l'espacement entre les lettres d'un mot.<br>
````css
    .ligne {
    letter-spacing : 10px;
    }   
````

<span style="color:#70F3EF;">word-wrap </span>, définit la césure d'un mot.<br>
Si un mot est contenu dans un bloc d'une taille définie et que celui-ci est plus grand que la taille du bloc, on peut contrôler comment le mot en bout de ligne doit s'afficher.
````css
    .ligne {
    word-wrap : break-word;
    }   
````
Avec <span style="color:#70F3EF;">break-word </span> le texte se mettra à la ligne arrivé en bout du conteneur.

<span style="color:#26f260;">**5. L'alignement en CSS.**</span><br>
<span style="color:#70F3EF;">text-align </span>, définit l'alignement horizontal du texte ou du contenu dans un bloc. Il existe plusieurs valeurs possibles pour cette propriété : left, right, center, justify.<br>
````css
    div {
    text-align: justify;
    }   
````

<span style="color:#70F3EF;">vertical-align </span>, définit l'alignement vertical du texte ou du contenu dans une ligne ou dans une cellule d'un tableau.
````css
    div {
    vertical-align: center;
    }   
````
Simuler des tableaux avec les propriétés display : table et table-cell.<br>
````css
    .conteneur {
    display: table;
    height: 200px;
    margin: 0 auto;
    width: 400px;
    margin-top: 20px;
    background: #f2f2f2;
    }

    .contenu-top {
    padding: 40px;
    text-align: center;
    display: table-cell;
    vertical-align: top;
    } 
````
<span style="color:#26f260;">**6. Les polices de caractères.**</span><br>
<span style="color:#70F3EF;">font-family </span>, définit la police d'écriture d'un élément.
````css
    #times {
    font-family: "Times New Roman", Times, serif;
    }

    #arial {
    font-family : Arial, Helvetica, sans-serif;
    }
````

<span style="color:#70F3EF;">font-weight </span>, définit la « graisse » pour le texte.<br>
- normal : valeur par défaut (ou 400 en valeur numérique)<br>
- bold : le texte est en gras (ou 700 en numérique)<br>
- lighter : équivaut à un niveau de graisse en moins par rapport à l'élément parent.<br>
- bolder : équivaut à un niveau de graisse en plus par rapport à l'élément parent.<br>
- De 1 – 1000 : valeur numérique, plus la valeur est élevée, plus la graisse du texte est élevée.
````css
    #times {
    font-width: 400;
    }

    #arial {
    font-width: 700;
    }
````

<span style="color:#26f260;">**7. Les couleurs**</span><br>
Il est possible de nommer les couleurs de plusieurs façons en CSS.<br>
Parmi ces notations on trouvera :<br>
- texte, par exemple red.<br>
- hexadécimale, par exemple #ff0000, indiquant par tranche de deux caractères (en base hexadécimale) respectivement la quantité de rouge, de vert et de bleu.<br>
- RGB, par exemple rgb(255, 0, 0), indiquant respectivement la quantité de rouge, de vert et de bleu<br>
- RGBA, par exemple rgba(255, 0, 0, 0.5), indiquant en plus la transparence à appliquer sur la couleur.

Il en existe d'autres, telles que la notation HSL, mais son utilisation est plus complexe.

<span style="color:#70F3EF;">color </span>, modifie la couleur d'un texte.
````css
    .red {
    color: #ff0000;
    }
````

<span style="color:#70F3EF;">background-color </span>, modifie le fond d'un texte, l'arrière fond.
````css
    .bg-red {
    background-color: #ff0000;
    }
````
<span style="color:#26f260;">**8. Le CSS des boîtes.**</span><br>

<span style="color:#26f260;">*La propriété display.*</span><br>
La propriété CSS display permet de gérer l'affichage d'un élément par rapport aux autres dans la page.<br>
- Avec la valeur <span style="color:#26f260;">*display: inline*</span>, les éléments vont être placés côte à côte sur la même ligne. L'élément ne prendra que la largeur nécessaire à l'affichage de son contenu, jusqu'à ce qu'il n'y ait plus d'espace en longueur. Les éléments suivants seront alors mis à la ligne. Les propriétés <span style="color:#26f260;">*width*</span> et <span style="color:#26f260;">*height*</span> n'ont aucun effet sur un élément inline.<br>
````css
    .inline {
    display: inline;
    }
````
- Avec la valeur <span style="color:#26f260;">*display: block*</span>, les éléments vont s'empiler les uns sur les autres. Un élément occupera donc une ligne (il prendra toute la largeur, peu importe la taille de son contenu), il ne sera jamais côte à côte avec un autre élément.
````css
    .block {
    display: block;
    }
````
- La valeur <span style="color:#26f260;">*display: inline-block*</span>, quant à elle, partage certaines caractéristiques des propriétés précédentes. En effet, elle est affectée par les propriétés permettant la modification de ses dimensions, et les éléments seront placés côte à côté jusqu'à remplir tout l'espace sur la ligne. Les propriétés <span style="color:#26f260;">*width*</span> et <span style="color:#26f260;">*height*</span> sont actives
````css
    nav li {
    display: inline;
    width: 150px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    border: 1px solid red;
    }
````
Il est possible de définir la hauteur et la largeur minimales d'un élément grâce aux propriétés <span style="color:#26f260;">*min-width*</span> et <span style="color:#26f260;">*min-height*</span>. Si la taille du bloc diminue, elle ne pourra pas être inférieure aux valeurs des propriétés <span style="color:#26f260;">*min-width*</span> et <span style="color:#26f260;">*min-height*</span>.<br>

De même qu'il est possible de définir la hauteur et la largeur maximales d'un élément avec les propriétés <span style="color:#26f260;">*max-width*</span> et <span style="color:#26f260;">*max-height*</span>.

La propriété <span style="color:#26f260;">*overflow*</span> permet de gérer la façon dont le contenu dépasse d'une boîte :

<span style="color:#26f260;">*overflow: hidden*</span> permet de le cacher, le contenu sera alors tronqué

<span style="color:#26f260;">*overflow: scroll*</span> fait apparaître les barres de défilement, qui permettront de scroller dans l'élément pour afficher le contenu

Par défaut, la propriété est <span style="color:#26f260;">*overflow: visible*</span>, ce qui implique que le contenu (s'il dépasse les limites de son conteneur) ne sera pas rogné.

<span style="color:#26f260;">*margin*</span> est la propriété qui permet de gérer l'espace le plus à l'extérieur de l'élément. Elle va déterminer l'espace entre l'élément et les autres : il s'agit de la marge externe de l'élément.

<span style="color:#26f260;">*border*</span> est la propriété qui représente la bordure directement autour de l'objet.

<span style="color:#26f260;">*padding*</span> est la propriété juste autour du contenu, qui permet de gérer les marges internes à l'élément.

Ces propriétés peuvent être manipulées et modifiées afin d'espacer des éléments, ou de donner plus de marge autour d'un contenu.

![img_2.png](img_2.png)

<span style="color:#26f260;">*Centrer horizontalement*</span>.
Utiliser la propriété de largeur <span style="color:#26f260;">*width*</span>, associée à la propriété et la valeur <span style="color:#26f260;">*margin: auto*</span>, permet de centrer horizontalement le contenu d'un élément.<br>

<span style="color:#26f260;">**9. Le poids des éléments, la spécificité.**</span><br>

De façon générale, plus un sélecteur est précis, plus celui-ci sera considéré comme prioritaire.<br>

Un sélecteur d'élément est peu spécifique : il cible tous les éléments d'un type donné dans la page, son score est donc faible.<br>

Un sélecteur de classe est plus spécifique : dans la page, il ne cible que les éléments dont l'attribut class a la valeur choisie, son score est donc plus important.<br>

Par exemple, si nous ciblons un élément en utilisant son nom de balise et sa classe, la règle CSS prioritaire sera la plus précise. Ce sera donc celle dont le sélecteur est composé du nom de la balise et de la classe. En effet, son poids sera plus important que si nous avions seulement donné la classe ou le nom de l'élément.<br>
````css
 p {
    color: blue;
    }

    .important {
    color: red;
    }

    p .important {
    color: orange;
    }
````
Le selecteur avec une class <span style="color:lightgreen;">*.important*</span> est prioritaire sur un sélecteur html <span style="color:lightgreen;">*p*</span>, le sélecteur + une class est encore plus prioritaire <span style="color:lightgreen;">*p .important*</span>.
````css
 p {
    color: blue;
    }

    .important {
    color: red !important;
    }

    p .important {
    color: orange;
    }
````
Avec <span style="color:#ffaa00;">*!important*</span> c'est lui qui devient le plus prioritaire.<br>

<span style="color:#26f260;">**10. L'héritage**</span><br>
Certains styles peuvent se transmettre d'éléments parents à éléments enfants : il s'agit d'héritage de styles CSS.<br>

Par exemple, la couleur du texte, la police ou encore l'alignement sont des propriétés qui vont se transmettre de parents vers enfants.<br>

C'est-à-dire que, si le parent possède une couleur d'écriture verte, alors les enfants qu'il contient seront également affichés en vert, sauf si nous réécrivons le style de l'enfant.<br>

Toutefois, toutes les propriétés CSS ne sont pas héritables : en effet, les règle sur les boîtes notamment (padding et margin par exemple) ne seront pas transposées aux enfants et ne s'appliqueront que sur le parent sélectionné.<br>

<span style="color:#26f260;">**11. Sélecteurs et combinateurs.**</span><br>

Avant de styliser un élément, il est important de le cibler en utilisant la méthode la plus juste.

Voici les principaux sélecteurs possibles et la manière de les utiliser :

Sélecteur universel : <span style="color:#ff0000;">*</span> va sélectionner tous les éléments du document.

Sélecteur d'élément : le nom de l'élément sans les chevrons va sélectionner tous les éléments de ce type.

Sélecteur par identifiant : l'attribut <span style="color:#ff0000;">id=" "</span> d'un élément va cibler l'élément possédant cet identifiant.
````css
    #id {
    color: #2cc0b4;
    font-style: italic;
    }
````
Sélecteur par classe : l'attribut <span style="color:#ff0000;">class=" "</span> d'un élément va cibler tous les éléments possédant cette classe.
````css
    .class {
    color: #2cc0b4;
    font-style: italic;
    }
````
Sélecteur d'attribut : va cibler tous les éléments possédant un attribut particulier.

Il est possible de préciser la sélection d'éléments en utilisant les combinateurs, soit en sélectionnant l'enfant direct d'un élément avec <span style="color:#ff0000;">">"</span>, 
````css
    div > p {
    color: #2cc0b4;
    font-style: italic;
    }
````
soit en sélectionnant tous les enfants d'un type descendant d'un élément avec un espace blanc.
````css
    div p {
    color: #2cc0b4;
    font-style: italic;
    }
````

Il est également possible de sélectionner le voisin direct d'un élément à l'aide du <span style="color:#ff0000;">"+"</span>
````css
    div + p {
    color: #2cc0b4;
    font-style: italic;
    }
````
ou de sélectionner les voisins d'un même type grâce au <span style="color:#ff0000;">"~"</span>.
````css
    div ~ p {
    color: #2cc0b4;
    font-style: italic;
    }
````

<span style="color:#26f260;">**12. Pseudo-classes.**</span><br>
Une pseudo-classe est une classe permettant de sélectionner un élément quand il est dans un état particulier.<br>
<span style="color:#26f260;">:active</span>. sélectionne l'élément lorsqu'il est activé, notamment par le clic utilisateur.
````css
    a:active {
        
    }
````
<span style="color:#26f260;">:hover</span> cible l'élément au survol de la souris.
````css
    a:active {
        
    }
````
<span style="color:#26f260;">:checked</span> cible l'élément lorsqu'il est coché.
````css
    a:checked {
        
    }
````
<span style="color:#26f260;">:focus</span> cible l'élément lorsque celui-ci est sélectionné par l'utilisateur.
````css
    a:focus {
        
    }
````
<span style="color:#26f260;">:visited</span> détermine le style d'un lien lorsque l'utilisateur l'a déjà visité.<br>
````css
    a:visited {
        
    }
````
<span style="color:#26f260;">**13. Pseudo-éléments.**</span><br>
Il est possible de sélectionner une certaine partie d'un élément grâce aux pseudo-éléments : le style ne s'appliquera pas à l'élément entier, mais seulement à la partie de l'élément ciblée.<br>
<span style="color:#26f260;">*::after*</span> permet de créer un élément interne juste après l'élément sélectionné. Il faut associer la propriété CSS content à ce pseudo-élément pour ajouter des éléments, comme une icône qui va suivre un lien pour indiquer qu'il est externe au site. Attention, le contenu de ce pseudo-élément n'est pas sélectionnable par l'utilisateur.

<span style="color:#26f260;">*::before*</span> pareil que <span style="color:#26f260;">*::after*</span>, mais pour <span style="color:#26f260;">*::before*</span> l'élément créé précédera l'élément sélectionné.

<span style="color:#26f260;">*::first-line*</span> permet de sélectionner la première ligne d'un paragraphe.

<span style="color:#26f260;">*::first-letter*</span> sélectionne la première lettre d'une phrase.

Ces pseudo-éléments s'utilisent après le sélecteur souhaité, suivi de "::pseudo-élément".
````css
    p::first-letter {

    }
````
<span style="color:#26f260;">**14. Position.**</span><br>

Le positionnement static c'est la valeur par défaut : les éléments se positionnent les uns à la suite des autres en suivant leur ordre d'écriture dans le document HTML.<br>
````css
    .static {
    position: static;
    } 
````
Le positionnement absolute permet de placer un élément où l'on souhaite sur la page en tenant compte de son parent.<br>

Le positionnement fixed est équivalent au positionnement absolu, mais l'élément concerné restera toujours visible au même endroit, même si l'on descend plus bas dans la page.<br>

Le positionnement relative permet de déplacer un élément par rapport à sa position initiale.<br>
En ajoutant des propriétés, notamment les propriétés top, left, bottom ou right, cela permet d'ajuster la position de l'élément par rapport à sa position initiale, dictée par les autres éléments autour de lui.<br>
````html
    <div class="blocRelativeGreen">
    .blocRelativeGreen
    </div>
    <div class="blocRelativePink">
        .blocRelativePink
    </div> 
````
````css
    .blocRelativeGreen {
    position: relative;
    height: 50px;
    background: #00BFBF;
    color: white;
    padding: 5px;
}
.blocRelativePink {
    position: relative;
    top: -25px;
    left: 25px;
    width: 400px;
    height: 50px;
    background: #FFAEC9;
    color: white;
    padding: 5px;
} 
````
![img_3.png](img_3.png)
<span style="color:#26f260;">**15. Float.**</span><br>
Pour placer un bloc par rapport au contenu principal, on utilise :

<span style="color:#00fffb;">*float: left*</span> pour le placer à gauche.<br>
````css
    .floatBlue {
    width: 200px;
    height: 70px;
    float: left;
    background-color: #5233FF;
    }
````
<span style="color:#00fffb;">*float: right*</span> pour le placer à droite.<br>

Pour empêcher un élément d'être placé après un élément flottant, on utilise :<br>
<span style="color:#00fffb;">*clear: none*</span> C'est la valeur par défaut. Pas de restriction, les éléments se positionneront à côté d'éléments flottants s'il y en a.<br>

<span style="color:#00fffb;">*clear: left*</span> pour empêcher de le placer à côté d'un élément en <span style="color:#00fffb;">*float: left*</span>.<br>

<span style="color:#00fffb;">*clear: right*</span> pour empêcher de le placer à côté d'un élément en <span style="color:#00fffb;">*float: right*</span>.<br>

<span style="color:#00fffb;">*clear: both*</span> pour les deux.Empêche un élément de se placer à côté d'éléments placés en <span style="color:#00fffb;">*float: left*</span> ou <span style="color:#00fffb;">*float: right*</span><br>

<span style="color:#26f260;">**16. Flexbox.**</span><br>

<span style="color:#26f260;">**17. Z-index.**</span><br>


<span style="color:#26f260;">**20. Récapitulatif des propriétés CSS.**</span><br>
<span style="color:#70F3EF;">font-familly</span>, police de caratères.<br>
<span style="color:#70F3EF;">text-align: center;</span> centre le texte.<br>
<span style="color:#70F3EF;">color: orange;</span> colorie le texte en orange.<br>
<span style="color:#70F3EF;">font-size</span>, taille de la police de caractères.<br>
<span style="color:#70F3EF;">class</span>, pour l'appeler en css ont met un point plus le nom de la class.<br>
<span style="color:#70F3EF;">font-weight</span>, pour la graisse des caratères.<br>
<span style="color:#70F3EF;">width</span>, largeur.<br>
<span style="color:#70F3EF;">height</span>, hauteur.<br>
<span style="color:#70F3EF;">background-color: lightgreen;</span> couleur de fond vert clair.<br>
<span style="color:#70F3EF;">border</span> bordure, épaiseur,solid,couleur.<br>
<span style="color:#70F3EF;">border-radius</span> arrondir les bord.<br>
<span style="color:#70F3EF;">padding</span> marge intérieur.  
<span style="color:#70F3EF;">marging</span> marge extérieur.  
<span style="color:#70F3EF;">display:inline-block</span> ,   
<span style="color:#70F3EF;">texte-decoration:underline</span>, souligné, none  = pas de décoration. <br>
<span style="color:#70F3EF;">backgroung-image:url de l'image</span>, permet d'importer une image.<br>
<span style="color:#70F3EF;">backgroung-repeat</span>,     <br>
<span style="color:#70F3EF;">backgroung-position</span>,  <br> 
<span style="color:#70F3EF;">backgroung-size transition:ease-in-out 0.8s</span>, transition<br>
<span style="color:#70F3EF;">box-shadow</span>, ombre.<br>
<span style="color:#70F3EF;">Pseudo class </span>, 
<span style="color:#70F3EF;">backgroung-repeat</span><br>
<span style="color:#70F3EF;">a:hover </span>, au survol de la souris.<br>

