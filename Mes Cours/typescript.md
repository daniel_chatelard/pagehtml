##  <div style="color: #26B260">**VIII. typescript**</div>
[Retour au sommaire](sommaire.md)

### <span style="color: #aaffff">**1. présentation nodejs.**</span>
<span style="color: yellow">*Node.js*</span>, est un moteur d'éxécution du langage javascript.On va pouvoir éxecuter du <span style="color: yellow">*javascript*</span> avec <span style="color: yellow">*Node.js*</span>, qui nous donne accés au développement d'application d' apis côté serveur.il est basé sur le moteur V8 de chrome.<br>
Lorsqu'on installation <span style="color: yellow">*Node.js*</span>, il installe aussi npm (node package management) c'est son gestionnaire de paquets.<br>

### <span style="color: #aaffff">**2. présentation npm.**</span>
<span style="color: yellow">*npm (node package management)*</span> c'est son gestionnaire de paquets, un paquet est un programme développé en javascript et qui est mis en avant sur la plateforme npm pour la communauté javascript.<br>
On peut créer nos propres paquets, il éxiste déja réact, angular et bien d'autres.<br>

### <span style="color: #aaffff">**3. présentation typescript.**</span>
C'est un langage de programmation développé par microsoft qui est gratuit et open source.il va rajouter un ensemble de fonctionnalité à javascript.<br>
Améliore la sécurité du code et le rend plus propre, plus lisible.

````markdown
Installation de typescript.

tapez: npm install -g typescript dans le terminal.
````
-g pour le mettre en global.là on a installer le transpiller.Pour l'utiliser on peut mettre la commande tsc.<br>
Avec typescript on peut typer les variables.
````markdown
transpiller le code typescript avec la commande (tsc).il créera un fichier javascript.

tsc index.ts
````
````typescript
    let var1: boolean = true;
    
````
### <span style="color: #aaffff">**4. configurer un package nodejs.**</span>

Créer un dossier script, puis créer un fichier index.html, ensuite aller dans le terminal et se mettre dans le dossier script.<br>
Ensuite tapez npm init, puis pllusieurs fois sur enter, remplir les champs si besoin.<br>
Après il va rajouter un package.json.
Dans un projet, on partage toujours les packages json en plus du fichier html, car ils ont les dépendances de notre projet.<br>
Ensuite la personne qui récupère les fichier devra faire un npm install, pour récupérer les dépendances du projet, qui va lui créer un dossier node_modules automatiquement.

````typescript
/*
{
  "name": "script",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {// ici on peut écrire des commandes.
    "test": "echo \"Error: no test specified\" && exit 1"
    // exemple:
   // "build": "npm build index.html"
    // pour éxécuter "build" on tape (npm run build) dans le terminal.
    },
      "author": "",
      "license": "ISC"
      "dependencies": {
          "express": "^4.17.1"
    }     
}
*/

````
### <span style="color: #aaffff">**5. basic types.**</span>
````typescript
    let a = 3;//Typage dynamique en javascript.

    // Typage en typescript d'un nombre.
    let b: number = 3;// ici on a typé la variable en number.

    // Typage d'une chaine de caractère.
    let str: string;
    str ="OK";
    
    // typage d'un booléen.
    let bool: boolean = true;
    bool = false;
    
    // Typage par soit un nombre, soit une chaine de caractère.
    let nb: number | string = 3;
    // ou un tableau ou etc...
    let nb1: [] | string | boolean;
    
    // On peut typer un tableau, en number, string ou booléen.
    let arr: number[];
    arr = [];
    arr.push(true);
    
    // Typer un objet.
    let obj: {
        name: string,
        age?: number | string};// ? pour optionnel
    obj = {name: "Jean", age: "12"};
````
### <span style="color: #aaffff">**6. functions.**</span>
````typescript
    // Typage des fonctions
    
    //déclaration d'une fonction nommée
    function multiply(nb1: number, nb2: number): number {
        return nb1 * nb2;
    }
    
    // déclaration d'une fonction anonyme.
    let multiply1 = function (nb1: number, nb2: number) {
    
    };
    // On peut typer la variable pour quelle retourne un nombre.
    let multiply2:(nb1:number, nb2: number) => number;
    multiply2 = function (nb1, nb2) {
        return nb1 * nb2;
    };

    multiply(1, 2);
    multiply1(1, 2);
    
    // autres exemple:
    function createUser(name: string, age: number, adresse?: string ):{name: string, age: number, adresse: string} {
        return {
            name,// est égal à name: name,
            age,// est égal à age: age,
            adresse// est égal à adresse: adresse
        };
    }
    
    //createUser( name:'Jean', age: 12, adresse: "Bld");
````
### <span style="color: #aaffff">**7. enums.**</span>
````typescript
    //énumérateurs, Enums

// Création d'un type avec plusieurs constantes.
    enum Direction {
        TOP = "123",
        BOTTOM = "327",
        RIGHT = "567",
        LEFT = "234",
    }
    
    //Typer une variable
    let userDirection: Direction = Direction.TOP;

    console.log(userDirection);
````
lancer le transpiller pour tester le code
````markdown
    tsc index.ts
````
Pour éxecuter le fichier index.js dans le terminal avec node.
````markdown
    node index.js

affichera 123
````
[doc-enum](https://www.typescriptlang.org/docs/handbook/enums.html#numeric-enums)
### <span style="color: #aaffff">**8. tuples.**</span>
Permet de typer une variable avec un tableau d'éléments fini.
````typescript
    let arr: [number[], string, number[]] = [[12], "ok", [4]];
`````
[doc-tuples](https://www.typescriptlang.org/docs/handbook/basic-types.html#tuple)
### <span style="color: #aaffff">**9. literal types.**</span>

````typescript
// Les types littéraux
    let transition: 'ease-in' | 'ease-out';
    transition = 'ease-in';
    
    function transformX(transition: 'ease-in' | 'ease-out'): void {
        
    }
    
    //transformX(transition:'ease-in');
````
[doc-literal](https://www.typescriptlang.org/docs/handbook/literal-types.html#string-literal-types)


### <span style="color: #aaffff">**10. types assertions.**</span>
````typescript
    let input: unknown = "24;"// formulaire page html
    let var1: string;

    var1 = input as string;
    // ou
    var1 = <string>input;
````
[doc-assertions](https://www.typescriptlang.org/docs/handbook/release-notes/typescript-2-5.html#type-assertioncast-syntax-in-checkjsts-check-mode)

### <span style="color: #aaffff">**11. interfaces.**</span>
````typescript
    // Les interfaces, typer des objets ou des classes.
    // déclarer un interface avec le mot clé (interface), puis un nom et des propriété.
    interface Ivehicule {
        model: string;
        km: number;
        move(): boolean;
        marque?: string;
    }
    // variable qui va implémenter notre interface.
    let car: Ivehicule;
    car = {
        model: 'BMW',
        km: 12,
        move: function () {
            return true;
        },
        marque: "OK"
    };

````

Comment typer une fonction dans une interface.

````typescript
    // c
    interface  MyFn {
        (): boolean;
    }
    
    let myFn: MyFn = function () {
        return 
    };
````
[doc-interface](https://www.typescriptlang.org/docs/handbook/interfaces.html)

### <span style="color: #aaffff">**12. class.**</span>

````typescript
    // classes

    class Car {
        
        model: string;
    
        constructor(model: string) {
            this.model = model;
        }
        // mot clé pour l'héritage
        public 
    }   

    // refaire exemple

    console.log(car.model);

````

[doc-classes](https://www.typescriptlang.org/docs/handbook/classes.html#classes)
### <span style="color: #aaffff">**13. parameter properties.**</span>



[doc-parameter-properties](https://www.typescriptlang.org/docs/handbook/classes.html#parameter-properties)
### <span style="color: #aaffff">**14. import export modules.**</span>


[doc-import export modules](https://www.typescriptlang.org/docs/handbook/modules.html#import-a-single-export-from-a-module)

### <span style="color: #aaffff">**15. ts config.**</span>



[doc-ts config](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html#tsconfig-bases)