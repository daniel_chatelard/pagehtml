##  <div style="color: #26B260">**X. React**</div>
[Retour au sommaire](sommaire.md)

### <span style="color: #aaffff">**1. Présentation.**</span>


### <span style="color : #aaffff">**2. Créer un nouveau composant.**</span>

Avec la méthode createElement.

````javascript
// j'enregistre dans la variable title.
const title =  React.createElement('h1', {}, 'Hello Word', 'Jean');

// je rend la variable title avec la méthode render()
ReactDOM.render(title, document.querySelector('#app'));
// j'affiche dans la console.
console.log(title);
````

React.createElement().
Premier argument son type, h1.
Deuxième argument options.
Les autres arguments qui suivent sont les enfants (props = propriété).

ReactDOM.render()
Pour afficher l'élément que j'ai créé, il faut utiliser :
ReactDOM avec la méthode render() 
En premier argument je met la variable
En deuxième argument je lui dit dans quel élément je veux l'insérer.
Il est conseillé de mettre querySelector

### <span style="color: #aaffff">**3. Syntaxe JSX.**</span>
Le JSX simplifie le développement, pour l'utiliser il faut un outil externe qui va nous compiler le code JSX en JS natif, pour qu'il soit compatible avec tout les navigateurs.<br>
Cet outil s'appelle babel disponible en ligne via un lien dans la doc.
La façon la plus simple et rapide de tester JSX dans votre projet est d’ajouter la balise (script) ci-dessous à votre page :

````javascript
<script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
````

![img_26.png](img_26.png)

Pour indiquer que le fichier est écrit par babel, il faut mettre dans type="text/babel"  

![img_27.png](img_27.png)
Code écris en html avec babel

````javascript
const title1 = <h1>Compteur : <span>0</span></h1>
````


#### <span style="color: #aaffff">**l'interpolation avec les accolades { }.**</span>
Ouvrir des accolades et mettre la variable à l'intérieur.
````javascript
let n =1
function render() {
    const title1 = <h1>Compteur : <span>{n}</span></h1>

    ReactDOM.render(title1, document.querySelector('#app1'));
}

render();

setInterval(() => {
    n++;
    render();
}, 1000);
````
Je peux aussi y mettre le retour d'une fonction(getString()).

````javascript
function getString() {
    return n + ' fois';
}
function render() {
    const title1 = <h1>Compteur : <span>{getString()}</span></h1>

    ReactDOM.render(title1, document.querySelector('#app1'));
}
````

Si je veux le mettre sur plusieurs ligne, il faut que j'insère un espace (white space) avec les accolades.

````javascript
    const title1 = <h1>Compteur : {' '}
            <span>{getString()}</span></h1>
````
On ne peut pas afficher un objet, mais ça valeur.
````javascript
// afficher un objet, créera une erreur.
const title = <h1>Compteur :
    <span>{{name: 'Jean'}}</span>
    </h1>
// par contre on peut afficher la propriété d'un objet.
const title = <h1>Compteur :
    <span>{{name: 'Jean'}.name}</span>
</h1>
````

En JSX, on doit avoir qu'une seule racine pour créer un élément, pour ça on va créer une div et mettre nos éléments dans cette div.Par contre la div s'affiche.

````javascript
// div conteint deux éléments enfants.
const title = <div>
    <h1>Compteur : <span>{n}</span></h1>
    <ul>
        <li>Mon Tag 1</li>
    </ul>
</div>
````
Si on veut que la div ne s'affiche pas, il faut utiliser les fragments.

````javascript
// On utilise React.Fragment ou (<>)
const title = <React.Fragment>
    <h1>Compteur : <span>{n}</span></h1>
    <ul>
        <li>Mon Tag 1</li>
    </ul>
</React.Fragment>
````

Ajuster autant de li qu'il y a de Tag.<br>
On va stocker dans une variable (tagsEl) un tableau d'éléments avec la méthode map().

````javascript
let n = 1;

function render() {
    let tags = [
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];
    
    const tagsEl =tags.map((tag) => {
        return <li>{tag}</li>
    });
    
    const title = <React.Fragment>
        <h1>Compteur : <span>{n}</span></h1>
        <ul>{tagsEl}</ul>
    </React.Fragment>

    ReactDOM.render(title, document.querySelector(#app));

}

render();

````
Il crée une erreur car React voudrait une key unique à  tags pour identifier la valeur. Rajout de l'attribut key.

````javascript
let n = 1;

function render() {
    let tags = [
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];
    // Ajout de l'attribut key avec l'index.
    const tagsEl =tags.map((tag, index) => {
        return <li key={index} >{tag}</li>
    });
    
    const title = <React.Fragment>
        <h1>Compteur : <span>{n}</span></h1>
        <ul>{tagsEl}</ul>
    </React.Fragment>

    ReactDOM.render(title, document.querySelector(#app));

}

render();

````

Comment ajouter un id en JSX.

````javascript
let n = 1;

function render() {
    let tags = [
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];
    // Ajout de l'attribut key avec l'index.
    const tagsEl =tags.map((tag, index) => {
        return <li key={index} >{tag}</li>
    });
    // ajout d'un id sur h1.
    const title = <React.Fragment>
        <h1 id={"title-"+n}>Compteur : <span>{n}</span></h1>
        <ul>{tagsEl}</ul>
    </React.Fragment>

    ReactDOM.render(title, document.querySelector(#app));

}

render();

````

Comment changer la class en JSX, il faut utiliser className.

````javascript
let n = 1;

function render() {
    let tags = [
        'Tag 1',
        'Tag 2',
        'Tag 3'
    ];
    // Ajout de l'attribut key avec l'index.
    const tagsEl = tags.map((tag, index) => {
        return <li key={index}>{tag}</li>
    });
    // ajout d'une className, si je veux quelle soit dynamique j'écris className={"class-"+n}
    const title = <React.Fragment>
        <h1 id={"title-" + n} className="title">Compteur :
            <span>{n}</span>
        </h1>
        <ul>{tagsEl}</ul>
    </React.Fragment>

    ReactDOM.render(title, document.querySelector(#app));
}

render();

````

### <span style="color: #aaffff">**4. Déclarer un composant.**</span>

React.Component
Pour créer un composant il faut qu'on l'appelle comme une balise html.

````javascript
function Compteur() {
    return <h1>Compteur : <span>0</span></h1>
    // création du composant Compteur.
}
    ReactDOM.render(<Compteur/>, document.querySelector(#app));

````
Je peux lui passer des paramètres via props.

````javascript
function Compteur(props) {
    return <h1>Compteur : <span>{props.name}</span></h1>
    // récupération de "Jean"
}
    ReactDOM.render(<Compteur name="Jean"/>, document.querySelector(#app));



````

On peut passer un texte entre la balise ouvrante et fermante.
Ce sera un enfant (children).

````javascript
// Première façon en utilisant une fonction.
function Compteur(props) {
    // pour afficher le texte enfant, il faut le mettre dans une div
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}
    // dès que l'on met du texte entre les balises Compteur, dans les props apparaît children.
    ReactDOM.render(<Compteur name="Jean">Jean est un élève</Compteur>, document.querySelector(#app));



````



````javascript
// Deuxième façon de créer un composant c'est avec une class
function CompteurFn(props) {
    
    return <div>
        <h1>Compteur : <span>{props.name}</span></h1>
        <p>{props.children}</p>
    </div>
}
    class Compteur extends React.Component {
        // et la méthode render()
        render() {
            return <h1>Competeur</h1>
        }
    }
    
    ReactDOM.render(<Compteur name="Jean">Jean est un élève</Compteur>, document.querySelector(#app));



````

passer des paramètres via props.

````javascript
function CompteurFn(props) {
    return <div>
        <h1>Compteur :
            <span>{props.name}</span>
        </h1>
        <p>{props.children}</p>
    </div>
}
    // Deuxième façon de créer un composant c'est avec une class
    class Compteur extends React.Component {
        // this.props.name, récupère la propriété name de Compteur égal à "Jean".
        render() {
            return <div>
                <h1>{this.props.name}</h1>
                <p>{this.props.children}</p>
            </div>
        }
    }
    
    ReactDOM.render(<Compteur name="Jean">Jean est un élève</Compteur>, document.querySelector(#app));


````
Soit on crée un composant avec une fonction, soit avec une class.

### <span style="color: #aaffff">**5. Component lifecycle.**</span>
Le cycle de vie d'un composant.

````javascript
// Montage
class Compteur extends React.Component {
    
    contructor(props) {
        //super execute le constructeur parent
        super(props);
        console.log('1');
        this.state = {ok: '1'};
    }
    static getDerivedStateFromProps(state) {
        console.log('2');
        return state;
    }
    
    
    render() {
        console.log('3');
        return <div>
            <h1>Compteur : <span>{this.props.name}</span></h1>
            <p>{this.props.children}</p>
        </div>
    }

    componentDidMount() {
        console.log('4');
    }
    
    // mise à jour
    componentDidUpdate() {
        console.log('5');
    }
    
    // Démontage.
    componentWillUnmount() {
        console.log('6');
    }
    
}

````
Documentation.<br>
https://fr.reactjs.org/docs/react-component.html

### <span style="color: #aaffff">**6. Component state.**</span>
Différence entre les props et les states.

Rappel:<br>
Création d'un composant fonctionnel.<br>
![img_82.png](img_82.png)<br>

Création d'un composant avec une classe.<br>
![img_83.png](img_83.png)<br>

Dans les deux cas, on peut passer des propriétés (props)mais au niveau d'un composant fonctionnel il n'aura pas d'état local. Pour cela il faut utiliser une classe.<br>

Un state c'est l'état local d'un composant.<br>

Initialiser un état (state).<br>
![img_84.png](img_84.png)<br>

Pour modifier le state il faut utiliser la méthode this.setState().<br>
![img_86.png](img_86.png)<br>
On modifie le name eu ajoutant un tiret et le nb à 1.<br>

Code complet de la classe.<br>
![img_87.png](img_87.png)<br>

Créer un intervalle, toutes les secondes je change le nombre (nb) avec la méthode setState.<br>
![img_88.png](img_88.png)<br> 

Les props sont faites pour être immutable, elle ne sont pas faites pour être changé.<br>


### <span style="color: #aaffff">**7. Gérer les évènements**</span>

Les événements de React sont nommés en camelCase plutôt qu’en minuscules.<br>
En JSX on passe une fonction comme gestionnaire d’événements plutôt qu’une chaîne de caractères.<br>
![img_85.png](img_85.png)<br>

This.increment est un référence à la fonction increment().
![img_89.png](img_89.png)<br>

il faut le lier avec la méthode bind().<br>
![img_90.png](img_90.png)<br>

Incrémenter le state.nb.<br>
![img_91.png](img_91.png)<br>

On peut aussi le faire avec une fonction fléchée.<br>
![img_93.png](img_93.png)

Afficher le state.nb.<br>
![img_92.png](img_92.png)<br>

On peux aussi l'afficher avec une fonction fléchée.<br>
![img_94.png](img_94.png)<br>
Documentation.<br>
https://fr.reactjs.org/docs/handling-events.html

### <span style="color: #aaffff">**8. Affichage conditionnels**</span>

Première manière de faire en assignant le bouton dans la variable (btnReset).<br>

````jsx
class Compteur extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {nb: props.defaultValue};
    }
    
    increment() {
        // execute la méthode setState avec la fonction arrow suivante qui prend en paramètre (state) et qui retourne un objet ({nb: state.nb + 1}).
        this.setState((state) => ({nb: state.nb + 1}));
    }
    
    render() {
        // Déclaration de la variable btnReset
        let btnReset;
        // condition
        if (this.state.nb > 0) {
            <!-- afficher un bouton réinitialiser dans le cas ou mon nombre est supérieur à zéro -->
            btnReset = <button>Réinitialiser</button>
        }
                
        return <div>
                <h1>{this.props.name}</h1>
                <p>{this.state.nb}</p>
                
                <button onClick={this.increment.bind(this)}>Incrémente</button>
            <!-- afficher le btnReset par interpolation grace aux accolades {btnReset }-->
            {btnReset}
        </div>;
    }
}
````

On peut aussi.<br>

````jsx
class Compteur extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {nb: props.defaultValue};
    }
    
    increment() {
        // execute la méthode setState avec la fonction arrow suivante qui prend en paramètre (state) et qui retourne un objet ({nb: state.nb + 1}).
        this.setState((state) => ({nb: state.nb + 1}));
    }
    
    // méthode reset() va mettre à jour le state en appelant setState qui prend en paramètre une fonction callback qui va retourner un nouvel objet.
    reset() {
        this.setState(() => {
            return {nb: 0};
        })
    }
    
    render() {
        // Déclaration de la variable btnReset
        let btnReset;
        // condition
        if (this.state.nb > 0) {
            <!-- afficher un bouton réinitialiser dans le cas ou mon nombre est supérieur à zéro -->
            btnReset = <button onClick={() => this.reset()}>Réinitialiser</button>
        }
                
        return <div>
                <h1>{this.props.name}</h1>
                <p>{this.state.nb}</p>
                
                <button onClick={this.increment.bind(this)}>Incrémente</button>
            <!-- afficher le btnReset par interpolation grace aux accolades {btnReset }-->
            {btnReset}
        </div>;
    }
}
````

Afficher le bouton décrémenter avec une autre méthode.<br>

````jsx
class Compteur extends React.Component {

    constructor(props) {
        super(props);
        
        this.state = {nb: props.defaultValue};
    }
    
    increment() {
        // execute la méthode setState avec la fonction arrow suivante qui prend en paramètre (state) et qui retourne un objet ({nb: state.nb + 1}).
        this.setState((state) => ({nb: state.nb + 1}));
    }
    
    // Déclarer la méthode reset() va mettre à jour le state en appelant setState qui prend en paramètre une fonction callback qui va retourner un nouvel objet.
    reset() {
        this.setState(() => {
            return {nb: 0};
        })
    }
    
    // Déclarer la méthode decrement() elle va mettre à jour le nb dans le state.
    decrement() {
        this.setState((state) => {
            
            if(state.nb >= 1) {
                return {nb: state.nb - 1};
                // ou condition ternaire
                //nb: state.nb >= 1 ? state.nb - 1 : 0
            }
            return 0;
        })
    }
    
    render() {
        // Déclaration de la variable btnReset
        let btnReset;
        // condition
        if (this.state.nb > 0) {
            <!-- afficher un bouton réinitialiser dans le cas ou mon nombre est supérieur à zéro -->
            btnReset = <button onClick={() => this.reset()}>Réinitialiser</button>
        }
                
        return <div>
                <h1>{this.props.name}</h1>
                <p>{this.state.nb}</p>
            <!-- condition à la volée avec l'opérateur logique && -->
            {(this.state.nb >= 1) && <button onClick={() => this.decrement()}>Décrémenter</button>}
            
                <button onClick={this.increment.bind(this)}>Incrémente</button>
            <!-- afficher le btnReset par interpolation grace aux accolades {btnReset}-->
            {btnReset}
        </div>;
    }
}
````
Documentation.<br>
https://fr.reactjs.org/docs/conditional-rendering.html




### <span style="color: #aaffff">**9. Lists key**</span>

````jsx

class Counter extends React.Component {

    constructor(props) {
        super(props);

        this.state = {nb: props.defaultValue};
    }

    increment() {
        // execute la méthode setState avec la fonction arrow suivante qui prend en paramètre (state) et qui retourne un objet ({nb: state.nb + 1}).
        this.setState((state) => ({nb: state.nb + 1}));
    }

    // Déclarer la méthode reset() va mettre à jour le state en appelant setState qui prend en paramètre une fonction callback qui va retourner un nouvel objet.
    reset() {
        this.setState(() => {
            return {nb: 0};
        })
    }

    // Déclarer la méthode decrement() elle va mettre à jour le nb dans le state.
    decrement() {
        this.setState((state) => {

            if(state.nb >= 1) {
                return {nb: state.nb - 1};
                // ou condition ternaire
                //nb: state.nb >= 1 ? state.nb - 1 : 0
            }
            return 0;
        })
    }

    render() {
        // Déclaration de la variable btnReset
        let btnReset;
        // condition
        if (this.state.nb > 0) {
            <!-- afficher un bouton réinitialiser dans le cas ou mon nombre est supérieur à zéro -->
            btnReset = <button onClick={() => this.reset()}>Réinitialiser</button>
        }

        return <div>
            <h1>{this.props.name}</h1>
            <p>{this.state.nb}</p>
            <!-- condition à la volée avec l'opérateur logique && -->
            {(this.state.nb >= 1) && <button onClick={() => this.decrement()}>Décrémenter</button>}

            <button onClick={this.increment.bind(this)}>Incrémente</button>
            <!-- afficher le btnReset par interpolation grace aux accolades {btnReset}-->
            {btnReset}
        </div>;
    }
}

class Acceuil extends React.Component {
    // initialiser un state
    state = {
        counters : [],
    };
    
    addcounter() {
        let counters = this.state.counters;
        // nombre aléatoire entre 0 et 10
        let defaultValue = Math.round(Math.random() * 10);
        
        counters.push({name: 'Compteur', defaultValue });
        reprendre à 4min50
        this.setState({counter: [...counters]});
    }
    
    render() {
        return <div>
            <button onClick={() => this.addcounter()}>Ajouter un comteur</button>
            <Counter name="Compteur 1" defaultValue={0}/>
        </div>         
                    
    }
}

ReactDOM.render(<Acceuil/>, document.querySelector('#app'));
````










### <span style="color: #aaffff">**10. Faire remonter la donnée**</span>
### <span style="color: #aaffff">**11. Create react app**</span>
### <span style="color: #aaffff">**12. Formulaire**</span>
### <span style="color: #aaffff">**13. Router**</span>
