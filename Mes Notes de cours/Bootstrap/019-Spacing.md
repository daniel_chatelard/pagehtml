### <div style="color: #26B260">**19. Spacing.**</div>
[Retour au sommaire](1-Sommaire.md)

[getbootstrap-spacing](https://getbootstrap.com/docs/5.0/utilities/spacing/)

<span style="color: yellow">*my*</span> pour margin, marge extérieur de l'axe y.  
<span style="color: yellow">*mx*</span> pour margin,marge extérieur de l'axe  x.  
<span style="color: yellow">*py*</span> pour padding,marge intérieur de l'axe y.  
<span style="color: yellow">*px*</span> pour padding,marge intérieur de l'axe x.
