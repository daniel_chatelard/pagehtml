
##  <div style="color: #26B260">**2. La grille Bootstrap**</div>
[Retour au sommaire](1-Sommaire.md)

La grille Bootstrap repose sur un système de classes CSS, avec un conteneur contenant des lignes, elles-mêmes composées de colonnes.<br>

Il y a également des classes CSS pour gérer les alignements horizontaux et verticaux ou l'ordre des colonnes.<br>

La grille de Bootstrap est une grille à 12 colonnes.<br>

La grille de Bootstrap dispose donc de douze colonnes et cinq points de rupture pour le responsive web design.<br>

Une colonne peut donc avoir une taille de 1/12, 2/12, ..., ou 12/12 de l'espace disponible.<br>