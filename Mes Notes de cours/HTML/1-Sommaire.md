# <div style="color: #26B260">**Sommaire Cours HTML**</div>
[Retour au sommaire](../Sommaire.md)

1. [Introduction au HTML](001-Introduction.md)
2. [Les Balises Principales](002-Les-Balises-Principales.md)
3. [Les Balises du corps](003-Les-Balises-du-corps.md)
4. [Les Chemins Relatif ou Absolus](004-Les-Chemins.md)
5. [Récapitulatif des balises](005-Récapitulatif-des-balises.md)

