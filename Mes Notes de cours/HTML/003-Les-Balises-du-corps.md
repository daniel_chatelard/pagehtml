
## <div id="body" style="color: #26B260">**3. Les balises du corps.**</div>
[Retour au sommaire](1-Sommaire.md)<br>
1) <span style="color: red">**body.**</span>  
   C'est le corps de la page qui contient les éléments qui seront visibles dans la page.,il est composé de trois partie.<br>
    - Le header,  où ce trouve le logo et la navigation
    - main ou div.
    - le footer, c'est le pied de page.(qui contient le copyright, lien des mentions légales, adresse de contact).

2) <span style="color: red">***Commentaire.***</span><br>
   ils s'écrivent entre un chevron ouvrant, point d'exclamation, deus tirets, le comentaire et se ferme par , deux tirets et un chevron fermant.
```html
   <!-- ceci est un commentaire -->
```

3) <span style="color: red">**Les titres**</span>**, il éxiste 6 niveaux de h1 à h6, h1 est le plus important

```html
<h1>Titre de Niveau1 Important</h1>
```