
### <div style="color: #26B260">**5.Opérateurs d'Affectation**</div>
[Retour au sommaire](1-Sommaire.md)<br>

nb += 3 => nb = nb + 3 <br>
nb -= 5 => nb = nb - 5 <br>
nb *= 2 => nb = nb * 2 <br>
nb /= 3 => nb = nb / 3 <br>
nb %= 4 => nb = nb % 4 Modulo, affiche le reste de la division.
