## <span style="color: #26B260">9.  Le ternaire :</span>
[Retour au sommaire](1-Sommaire.md)<br>

L'opérateur ternaire <span style="color: #ff0000">( ? : )</span> est le seul opérateur à comporter trois opérandes. Une expression ternaire se définit la plupart du temps sur une seule ligne, et vérifie si une condition est vraie ou fausse.<br>
``````javascript
    let x = 0;
    let y = 9;
    let isEqual;

    // Cette condition...
    if (x == y) {
        isEqual = true
    } else {
        isEqual = false
    }

    // ... est équivalente à cette condition ternaire
    x == y ? isEqual = true : isEqual = false
``````
````javascript
    let lastname = 'Brassens';
    let genre = 'femme';

    console.log((genre == 'femme' ? 'Mme ' : 'M ') + lastname);
````