
### <div style="color: #26B260">**3. Opérateurs de comparaison**</div>
[Retour au sommaire](1-Sommaire.md)<br>

égalité simple, compare seulement la valeur avec  ==<br>
égalité strict, compare la valeur et le type avec ===<br>
l'inégalité avec !==<br>
strictement inférieur avec < <br>
strictement supérieur avec > <br>
inférieur ou égal avec <= <br>
supérieur ou égal avec >=
