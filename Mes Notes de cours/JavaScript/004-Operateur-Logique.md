
### <div style="color: #26B260">**4.Opérateurs Logique**</div>
[Retour au sommaire](1-Sommaire.md)<br>

le ET logique avec && <br>
le OU logique avec || (altGr 6) <br>
le non logique avec ! exemple: non true, !true
```javascript
    let operat = true;
    console.log('non logique de true:', !true);
```