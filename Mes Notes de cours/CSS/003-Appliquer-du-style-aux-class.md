
<span style="color:#26f260;">**3. Appliquer du style aux class.**</span><br>
[Retour au sommaire](1-Sommaire.md)<br>

Pour associer un style à une classe, on utilise le nom de la classe précédé d'un point.
````css
    .maclasse {
        color: green;
    }
````

Il est possible de cibler élément avec <span style="color:#70F3EF;"># et un identifiant</span> (attribut <span style="color:lightgreen;">id=" "</span>), cet identifiant est unique par page.

````css
    #identifiant {
        color: green;
    }
````