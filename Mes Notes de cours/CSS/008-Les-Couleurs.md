
<span style="color:#26f260;">**8. Les couleurs**</span><br>
[Retour au sommaire](1-Sommaire.md)<br>

Il est possible de nommer les couleurs de plusieurs façons en CSS.<br>
Parmi ces notations on trouvera :<br>
- texte, par exemple red.<br>
- hexadécimale, par exemple #ff0000, indiquant par tranche de deux caractères (en base hexadécimale) respectivement la quantité de rouge, de vert et de bleu.<br>
- RGB, par exemple rgb(255, 0, 0), indiquant respectivement la quantité de rouge, de vert et de bleu<br>
- RGBA, par exemple rgba(255, 0, 0, 0.5), indiquant en plus la transparence à appliquer sur la couleur.

Il en existe d'autres, telles que la notation HSL, mais son utilisation est plus complexe.

<span style="color:#70F3EF;">color </span>, modifie la couleur d'un texte.
````css
    .red {
    color: #ff0000;
    }
````

<span style="color:#70F3EF;">background-color </span>, modifie le fond d'un texte, l'arrière fond.
````css
    .bg-red {
    background-color: #ff0000;
    }
````
