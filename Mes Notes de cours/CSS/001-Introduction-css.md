
##  <div style="color: #26B260">**1. Introduction au CSS**</div>
[Retour au sommaire](1-Sommaire.md)<br>

Dans les années 2000, Hakon Wium Lie proposa un nouveau langage permettant d'habiller les sites à l'aide de propriétés : le CSS (Cascading Style Sheets, qui signifie « feuilles de style en cascade »).<br>

````css
    /* Permet d'indiquer que l'on cible les titres h1 */
    h1 {
    /* donne l'instruction d'afficher le texte de la couleur définie, à savoir rouge */
    color: red;
        /* commentaire en CSS */
}
````









