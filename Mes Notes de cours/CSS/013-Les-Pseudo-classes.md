
<span style="color:#26f260;">**13. Pseudo-classes.**</span><br>
[Retour au sommaire](1-Sommaire.md)<br>

Une pseudo-classe est une classe permettant de sélectionner un élément quand il est dans un état particulier.<br>
<span style="color:#26f260;">:active</span>. sélectionne l'élément lorsqu'il est activé, notamment par le clic utilisateur.
````css
    a:active {
        
    }
````
<span style="color:#26f260;">:hover</span> cible l'élément au survol de la souris.
````css
    a:active {
        
    }
````
<span style="color:#26f260;">:checked</span> cible l'élément lorsqu'il est coché.
````css
    a:checked {
        
    }
````
<span style="color:#26f260;">:focus</span> cible l'élément lorsque celui-ci est sélectionné par l'utilisateur.
````css
    a:focus {
        
    }
````
<span style="color:#26f260;">:visited</span> détermine le style d'un lien lorsque l'utilisateur l'a déjà visité.<br>
````css
    a:visited {
        
    }
````
