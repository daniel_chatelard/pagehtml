console.log('Sélectionner un ou des éléments');

window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM CHARGER !');

    // écouter le click sur un bouton.
    //Sélectionner le bouton avec getElementById.
    const btnEl = document.getElementById('createSquare');
    console.log(btnEl);// j'affiche pour vérifier.

    // je crée une référence.
    const redSquareContainerEl = document.getElementById('redSquare');


    // pour ajouter une écoute d'événement, j'utilise la méthode addEventListener.
    // en premier argument je doit donné le type d'événement que je veux écouter, ici (click).
    // en deuxième argument ce sera la fonction callback.
    btnEl.addEventListener('click', function () {
       // console.log('click', this.innerText); avec this je vois la référence de mon événement qui a été cliqué et je peut lui rajouter du text avec innerText pour changer son texte.
        console.log('click');

        // appeler la fonction creatSquare pour créer mon carré.
        //const square = createSquare();
        //console.log(square);
        // j'appel ma référence et je lui ajoute un enfant square
        //redSquareContainerEl.appendChild(square);

        // raccourci des lignes au dessus.
        redSquareContainerEl.appendChild(createSquare());
    });

    // écouter un événement sur la fenêtre window sur la souris.
    //window.addEventListener('mousemove', function (event) {
    //    console.log(event);


});

// créer une fonction qui crée un élément à qui on ajoute la class square et on va retourner cet élément de la fonction.
// cette fonction à chaque fois que l'on va l'appeller, elle va créer un carré.

    function createSquare() {
        const el = document.createElement('div');
        el.classList.add('square');
        return el;
    }