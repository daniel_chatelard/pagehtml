console.log('Sélectionner un élément grâce à son id');

window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM CHARGER !');
    console.log(document);// va représenter le document de la page html.

    // récupérer l'élément grâce à son id
    const square = document.getElementById('sqr');// récupérer dans une constante.
    console.log(square);// afficher la constante dans la console.
});