console.log('Sélectionner un ou des éléments');

window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM CHARGER !');

    const square = document.createElement('section');//crée un élément section.

    // ajout de la class square.
    square.classList.add('square');

    // rajouter du texte à notre élément avec innerText, ou pour y accéder..
    //square.innerText = 'Ok';

    // insérer du html dans un élément, utilisé innerHTML, sera insérer comme un enfant.
    //square.innerHTML = '<strong>Ok</strong>';

    // ajouter l'élément dans notre page

    // on doit d'abord récupérer l'élément sur lequel on veut le rajouter ici body, puis appendChild(), puis notre élément à ajouter en premier paramètre (square).

    document.body.appendChild(square);

    // ajouter container
    let container = document.createElement('div');// création d'une div

    container.innerText = 'Container';//ajout du texte dans container.

    document.body.appendChild(container);// ajouter container dans body.

    //ajouter un enfant à container
    container.appendChild(square);

    console.log(square);

});