

console.log('index.js');// affiche dans la console index.js

console.log(document.querySelector('h1')); // sélectionner le titre h1.

console.log(window);// sélectionner aussi via window.

// avant de manipuler le dom, il faut s'assurer qu'il est bien charger et écrire votre script dans l'une des deux méthode.
// première méthode pour s'assurer que le dom est chargé
window.onload = function (){
    console.log('4');
    console.log(document.querySelector('h1')); // sélectionner le titre h1.
};

// deuxième méthode pour s'assurer que le dom est chargé
window.addEventListener('DOMContentLoaded', function () {
    console.log('5');
    console.log(document.querySelector('h1')); // sélectionner le titre h1.
})