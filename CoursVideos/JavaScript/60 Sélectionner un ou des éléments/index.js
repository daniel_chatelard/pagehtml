console.log('Sélectionner un ou des éléments');

window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM CHARGER !');

    // sélectionne l'élément qui à la classe square bigSquare.
    //const square = document.querySelector('.square.bigSquare');

    //avec #, on peut sélectionner un (id = "sqr").
    // const square = document.querySelector('#sqr');//

    // pour rechercher plusieurs éléments.
    // sélectionne un élément avec la class="div square".
    // const squares = document.querySelectorAll('div.square');

    //On peut sélectionner un élément avec son tag ('section').
    //const squares = document.querySelectorAll('section');

    // sélectionner une class ="div square bigSquare.
    const squares = document.querySelectorAll('div.square.bigSquare');
    //console.log(square);
    console.log(squares);
});