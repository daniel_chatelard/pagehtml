console.log('Sélectionner un ou des éléments');

window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM CHARGER !');

    const square = document.querySelector('.square');

    // modifier son style
    square.style.backgroundColor = 'yellow';
    square.style.border = '1px solid grey';

    // déplacer un élément
    //square.style.position = 'fixed';
    //square.style.top = '0px';

    // déplacer un élément toute les 0.1 secondes (100 ms) verticalement.
    //let positionY = 0;

    //setInterval(function () {
    //    positionY++;
    //    square.style.top = positionY + 'px';
    // }, 100);

    // déplacer un élément toute les 0.1 secondes (100 ms) horizontalement.
    //let positionX = 0;

    //setInterval(function () {
       // if (positionX >= 500) {
       //     square.style.position = 'relative';
       // }
        //positionX = positionX + 10;
        //square.style.left = positionX + 'px';
    //}, 100);

    // changer de couleur en scintillant.
    let i =0;
    setInterval(function () {
            square.style.backgroundColor = i % 2 ? 'red' : 'yellow';
            i++;
    }, 1000);


    console.log(square);
});