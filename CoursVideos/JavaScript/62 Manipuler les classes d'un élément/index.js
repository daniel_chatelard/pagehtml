console.log('Sélectionner un ou des éléments');

window.addEventListener('DOMContentLoaded', function () {
    console.log('DOM CHARGER !');

    const square = document.querySelector('.square');
    console.log(square);

    let i = 0;
    setInterval(function () {
        if (i % 2) {// si pair, condition pour faire le scintillement.
            square.classList.add('bigSquare'); // ajouter une classe bigSquare.(ou plusieurs)
        } else {
            square.classList.remove('bigSquare'); // supprimer une classe bigSquare.(ou plusieurs)
        }
        i++;
    }, 1000);
});